library(ggplot2)

setwd("/Users/tomek/prj/ngram-word-pred/analysis/02-file-size-uncomp")

size             <- read.table("data/size.txt", header=T, sep=" ")
size$size_mb     <- size$size / 1024^2
size$log_size    <- log(size$size)
size$log_size_mb <- log(size$size_mb)

size$n.fac <- as.character(size$n)
size$n.fac[which(size$n == 3)] <- "3-grams"
size$n.fac[which(size$n == 4)] <- "4-grams"
size$n.fac[which(size$n == 5)] <- "5-grams"
size$n.fac <- factor(size$n.fac, levels=c("3-grams", "4-grams", "5-grams"))  # fix the order

img <- ggplot(size, aes(x=log_size_mb, color=factor(n))) +
    geom_density       () +
    ggtitle            ("Google Ngram File Size (English; Uncompressed)") +
    scale_x_continuous (name="file size [B]", limits=c(log(.001), log(1000000)), breaks=c(log(.001), log(.01), log(.1), log(1), log(10), log(100), log(1000), log(10000), log(100000), log(1000000)), labels=c("1k", "10k", "100k", "1M", "10M", "100M", "1G", "10G", "100G", "1T")) +
    labs(color="n") +
    theme_bw()

ggsave(file=paste("out/file-size-by-n.png", sep=""), plot=img, units="in", width=8, height=4, dpi=300)
