# N-gram Word Predictability
A command-line utility for computing word predictability based on n-grams.


## Abstract
Word predictability is an often-desired variable in psycholinguistics.  Obtaining it for small samples of text (e.g., sentences) is relatively straightforward and is typically done with the cloze task.  Unfortunately, it is impractical or even impossible to set up a cloze task for text corpora studies that often require entire books.  One of the solutions to this problem is to estimate word predictability computationally.  This repository presents an n-gram based approach of achieving just that while using the largest n-grams dataset available.  The software presented here should be especially useful for corpus analyses in psycholinguistics and when storage space is a limitation.


## Contents
- [Features](#features)
- [Details](#details)
  * [N-grams](#n-grams)
  * [Word Predictability](#word-predictability)
  * [Cloze Predictability](#cloze-predictability)
  * [N-gram Predictability](#n-gram-predictability)
  * [Google Ngram Data](#google-ngram-data)
  * [Input and Output Files](#input-and-output-files)
  * [Storage](#storage)
  * [Speed](#speed)
  * [Memoization](#memoization)
  * [Parallel Execution](#parallel-execution)
  * [Part-of-Speech Tagging](#part-of-speech-tagging)
  * [Logging](#logging)
  * [Customizing](#customizing)
  * [Pushover Notifications](#pushover-notifications)
  * [Database-Centric Approach](#database-centric-approach)
  * [Hardware](#hardware)
  * [Pegasus Integration](#pegasus-integration)
  * [Multilingual Support](#multilingual-support)
- [Dependencies](#dependencies)
- [Usage Example](#usage-example)
  * [Problem Description](#problem-description)
  * [Computing Word Predictability](#computing-word-predictability)
- [Setup](#setup)
- [Command Reference](#command-reference)
- [References](#references)
- [Citing](#citing)
- [License](#license)


## Features
- Computes word predictability based on bi-, tri-, 4-, and 5-grams
- Uses the largest n-gram dataset available ([Google Ngram](http://storage.googleapis.com/books/ngrams/books/datasetsv2.html))
- Automatically downloads all necessary n-gram files
- Caches all intermediate computations (i.e., implements memoization) to minimize re-computation costs (due to either errors or manual interruptions)
- Minimizes storage space utilization by using compress data but can work with uncompressed data as well
- Supports parallel processing
- Supports progress updates based on [Pushover](https://pushover.net) notifications
- No support for part-of-speech (POS) tagging at this point


## Details
### N-grams
Shamelessly quoting the [Wikipedia](https://en.wikipedia.org/wiki/N-gram) (it's just well-written, folks):

> In the fields of computational linguistics and probability, an _n-gram_ is a contiguous sequence of n items from a given sample of text or speech.  The items can be phonemes, syllables, letters, words or base pairs according to the application.  The n-grams typically are collected from a text or speech corpus.

In this repository, I focus on n-grams of successive words and use [Google Ngram data](http://storage.googleapis.com/books/ngrams/books/datasetsv2.html).

### Word Predictability
_Word predictability_ measures how unsurprising the upcoming word is given its context.  For example, consider the two following sentences:
```
It often snows in the winter.
It often snows in the Himalayas.
```
Even though these sentences begin the same, the word `Himmalayas` is likely more unexpected than the word `winter`.  If that was so indeed, then `winter` would be _more predictable_ or have a higher value for word predictability.

Statistically speaking, word predictability can be defined as the conditional probability of a word in a sentence given all the preceding words in that sentence.  That is, word predictability of the last word in each of the two example sentences above is:
```
pred(winter)    = p(winter    | It often snows in the)
pred(Himalayas) = p(Himalayas | It often snows in the)
```
More generally:
```
pred(word) = p(word|context)
```
where ```context``` are the preceding words (either all or only some).  This is the approach taken in this repository.

### Cloze Predictability
The _cloze task_ (Taylor, 1953) is used to estimate the predictability of every word (or only some words) in text.  The task is completed by humans which guess the identity of the upcoming word while having access to all the preceding words.  After they make their guess, the actual word is revealed and the task continues with the next word.  When estimated with the cloze task word predictability is sometimes called _cloze predictability_.  Cloze predictability is often considered a gold standard for word predictability because it is assessed by people who, at least ideally, are (statistically) interchangeable with the subjects of subsequent experiments that use that measure.

### N-gram Predictability
While generally preferred, it may be impractical or even impossible to obtain cloze predictability.  That is the case in corpus analyses because they utilize large volumes of text (sometimes entire books).  _N-gram predictability_ is a computational approach to estimating word predictability in those difficult cases.  The present software calculates it in the following way:
```
pred(word) = p(word|context) = count(context + word) / count(context),
```
where `context` are the preceding words and `count` is the number of times the particular sequence of words appears in Google Ngram dataset.  Note that both `context` and `context + word` are n-grams, `context` being the shorter of the two.  Word predictability as defined above is a number between `0` and `1` because:
```
count(context+word) <= count(context),
```
that is, the numerator quantifies a more strict condition than the denominator.  The software outputs `-1` if `context` does not appear in the dataset (i.e., when `count(context) = 0`).

### Google Ngram Data
The Google Ngram data has been created by processing millions of books and chunking them into one, two, three, four, or five consecutive words (or rather strings of characters, because they don't need to be words per se; Michel et al., 2011).  Part-of-speech tagging was part of that process (Lin et al., 2012).  I suggest reading the two articles as well as familiarizing yourself with information published on [Google Ngram Viewer](https://books.google.com/ngrams/info) and [Google Ngram data](http://storage.googleapis.com/books/ngrams/books/datasetsv2.html) Web pages.

#### Individual File Sizes
Google Ngram data is organized into languages.  Each language is divided by n-gram length: Uni-, bi-, tri, 4-, and 5-grams.  Each of those n-gram dataset is further split into files based on the first two letters of n-grams they contain.  For example, all n-grams contained in the file `googlebooks-eng-all-3gram-20120701-th.gz` begin with the letters `th` and no other file will have n-grams like that.  Below, I only focus on English tri-, 4- and 5-grams, but the software will work with bi-grams as well.

<p align="center"><img src="analysis/01-file-size-comp/out/file-size-by-n.png" width="80%" alt="Google Ngram File Size Distribution (English; Compressed)" /></p>
<p align="center"><b>Figure 1.</b> Distribution of the compressed Google Ngram file sizes for the English language broken down by n-gram size.</p>

<p align="center"><img src="analysis/02-file-size-uncomp/out/file-size-by-n.png" width="80%" alt="Google Ngram File Size Distribution (English; Uncompressed)" /></p>
<p align="center"><b>Figure 2.</b> Distribution of the uncompressed Google Ngram file sizes for the English language broken down by n-gram size.</p>

The sizes of individual ngram files vary substantially as a result of uneven distribution of letters making up words in English.  For example, fewer words begin with `qa` than with `aq`.  For space and transfer efficiency, all files Google makes available are compressed.  Figure 1 provides some insight into the size of those files; it shows the distribution of sizes of compressed file sizes.  As is evident on Figure 2, uncompressing the data moves that distribution to the right by a substantial amount that seems close to an order of magnitude.

#### Automatic Download
The present software automatically downloads all necessary Google Ngram files whenever they are needed, but they can also be downloaded manually like so:
```sh
./do-word-pred download eng 3 aq 0 0
```
Executing the following command will download all the tri-, 4-, and 5-grams files for English in one loooooong run:
```sh
./do-word-pred download-all 0 0
```
To download languages other than English or to tailor the list of n-gram files to a particular application (e.g., exclude the `th` file), the following variables in the [`bin-fs/do-word-pred`](bin-fs/do-word-pred) shell script need to be changed:
```sh
download_all_lang=(eng)
download_all_n=(3 4 5)
download_all_letters=(a_ aa ab ac ...)
```
The [Command Reference](#command-reference) section located towards the end of this document contains descriptions of the two download commands.

#### Dataset Size
The total size of uncompressed English dataset I have used myself (which includes all tri-, 4-, and 5-grams) is about 17TB (compressed is a little under 4TB).  To get a feel for this size, assume that a good quality one-hour long TV series episode (e.g., _Game of Thrones_) takes 1GB of space.  That's about 17,000 _Game of Thrones_ episodes.  If a new episode aired once a week, that'd be about 326 years.  If it aired daily, that'd be about 46 years.

### Input and Output Files
This software will fall short of its potential (i.e., the processing times will be much higher than necessary) unless certain important instructions are adhered to.  Namely, the set of n-grams used to compute word predictability needs to be partitioned into input files in a very particular but logical way.  More specifically, all input n-grams starting with the same two letters (e.g., `th`) need to be in the same file (e.g., `eng-3-th-in`) and should not appear in any other input file.  That ensures that every Google Ngram file is processed only once.  Because interacting with those files takes the bulk of the total computation time, this results in significant time gains.    Furthermore, n-grams in each input file should be sorted alphabetically which will further increase the performance (although to a far lesser degree) because certain information can be reused for similar n-grams but only if they are adjacent.  Expanding on this reasoning, it is advantageous to cumulate as many input n-grams as possible.  For example, combining several books into one computational batch will result in significant decreases in computation time compared to processing each book separately.

Several output files are created as a result of word predictability computation.  Table 1 lists the names and descriptions of the input file and all the output files; all those files begin with the prefix `<lang>-<n>-<letters>-`.  For example, `eng-3-qa-in` is a valid input file name and the associated output file would be named `eng-3-qa-out`.

<p align="center"><b>Table 1.</b> Names and descriptions of the input and output files (all filenames are prefixed with '&lt;lang&gt;-&lt;n&gt;-&lt;letters&gt;-').</p>

| File   | Description |
| :----- | :---------- |
| `data` | The part of the Google Ngram file that is necessary for solving the input file |
| `grep` | GREP regular expressions used to excise the relevant parts of the Google Ngram file |
| `hist` | History of all word predictability computations (i.e., the cached or memoized values that will be used in future runs to conserve time) |
| `in`   | The input file |
| `out`  | The output file |
| `time` | Duration of computations info |

See the [Usage](#usage) section later in this document for a complete example which includes an input file and all output files.

### Storage
An important feature of the present software is the optimization of storage space.  Storing the entire uncompressed English Google Ngram dataset is likely to be problematic for most regular computer setups.  To address this problem, this software keeps the individual n-gram files compressed and deflates the ones it needs on the fly.  If storage space is not a limitation, uncompressed files can be used just as easily; the only change necessary to the [`bin-fs/do-word-pred`](bin-fs/do-word-pred) shell script is `do_use_comp=0`.  It should be noted, however, that according to my quick and unsystematic tests, using compressed data sometimes results in savings in both space and time (spacetime?).

### Speed
The [philosophy behind the UNIX](https://en.wikipedia.org/wiki/Unix_philosophy) operating system's [command-line utilities](https://en.wikipedia.org/wiki/List_of_Unix_commands) is to solve small problems and do it well.  That way, simple but robust tools can be composed together to solve bigger problems.  Owing it in no small part to their simplicity, those command-line tools are also very fast, which is the main reason I have chosen the shell script approach.

### Memoization
Computing word predictability of large number of words can be a lengthy operation easily taking hours or days.  To maximize the use of that time, this software caches all complete intermediate results (e.g., all computed word predictabilities) to make error recovery less costly.  A simple restart (after fixing the errors) is all that is necessary.  Moreover, computation can safely be interrupted at any point; when restarted, the software will pick up from the last stable point (for each of the parallel processes, if applicable).

### Parallel Execution
This software supports parallel execution.  However, because of the heavy storage utilization, the system will become I/O bound quickly if too many processes are started.  Experimentation to find the number that will be optimal for a particular configuration is recommended.

### Part-of-Speech Tagging
Part-of-speech tagging (POS) is not supported.  This may change in the future, but at this point all POS-tagged n-grams from Google Ngram files are ignored.  The regular expressions in the [`bin-fs/do-word-pred`](bin-fs/do-word-pred) shell script can be altered to account for POS-tagged n-grams.  However, an excellent understanding of Google Ngram data and an expert command of regular expressions is required to properly do this kind of work.

### Logging
The software keeps logs of all downloads (`bin-fs/log/download.log`) and all word predictability computations (`bin-fs/log/proc.log`).

### Customizing
Certain aspects of this software can and likely should be customized by editing the [`bin-fs/do-word-pred`](bin-fs/do-word-pred) shell script.  The most important one are the location of Google Ngram files (i.e., `dir_data_comp` and `dir_data_uncomp`) and whether or not to use compressed files (i.e., `do_use_comp`).  Look for the following self-explanatory lines at the top of the script:
```sh
dec_n_pred=10  # number of word predictability decimals

do_use_comp=1     # use compressed data? (i.e., should the data be deflated before use)
do_keep_uncomp=1  # keep uncompressed files? (in effect only if ${do_use_comp} above is 0)

dir_data_comp=../data/comp      # compressed
dir_data_uncomp=../data/uncomp  # uncompressed
dir_work=work
dir_log=log

file_log_proc=$dir_log/proc.log
file_log_download=$dir_log/download.log
```

### Pushover Notifications
It is always good to know the status of long-running computations and the computations performed by the present software are no exception.  These computation can take many hours and therefore waiting around the computer until they finish is impractical.  To free the user from the burden of periodic manual checking, the script can send [Pushover](https://pushover.net) notifications upon completion of processing of every input file.  To enable that feature, a script named `bin-fs/private` needs to be created with the following content:
```sh
pushover_usr_key="..."
pushover_app_key="..."
pushover_snd="pianobar"
pushover_dev="phone"
pushover_host="dedicated-supercomputer"
```
Often, the shell script will be tested on the local machine but the actual computations will be eventually scheduled on another computer.  To prevent the script from sending largely useless and annoying notifications during testing, the script sends them only from the machine with the specified hostname (`pushover_host`).  On the receiving end, only the specified device (e.g., a mobile phone; `pushover_dev`) will be notified.

### Database-Centric Approach
The [`bin-db/`](bin-db/) directory contains the original implementation of the n-gram predictability software which is based around PostgreSQL and Node.js.  Unfortunately, that solution proved too slow for the actual data which prompted the development of the current shell script.  Perhaps that code will be of some use to someone; it certainly can handle datasets smaller than Google Ngram.  I benchmarked many important implementation decisions and the results of those tests as well as my conclusions can be found in [`bin-db/tests.txt`](bin-db/tests.txt).

### Hardware
For reference, here are the specs of the system used to run the present software:

- Intel Xeon E5-2630 v2 Processor (Six Core HT, 2.6GHz Turbo, 15MB) x2
- 32GB of RAM (4-8-4 x2; DDR3 1866Mhz ECC)
- 128 GB SSD (root on ZFS)
- 1TB HDD x2 (ZFS mirror; RAID 1 equivalent)
- FreeBSD 10

Four external 4TB drives connected through USB 3.0 and stripped into one ZFS pool (RAID 0 equivalent) served as the storage for Google Ngram data.  The following commands create such a pool on FreeBSD:
```sh
gpart create -s gpt da0
gpart create -s gpt da1
gpart create -s gpt da2
gpart create -s gpt da3

gpart add -a 4k -b 1M -t freebsd-zfs -l usb0.0 da0
gpart add -a 4k -b 1M -t freebsd-zfs -l usb0.1 da1
gpart add -a 4k -b 1M -t freebsd-zfs -l usb0.2 da2
gpart add -a 4k -b 1M -t freebsd-zfs -l usb0.3 da3

zpool create usb /dev/gpt/usb0.0 /dev/gpt/usb0.1 /dev/gpt/usb0.2 /dev/gpt/usb0.3

zfs set compression=lz4 usb
zfs set mountpoint=/mnt/usb usb
```
The compression ratio on the `usb` dataset which contained both compressed and uncompressed Google Ngram data (English tri-, 4-, and 5-grams) was 4.62.

### Pegasus Integration
This software integrate with [Pegasus](http://gitlab.com/ubiq-x/pegasus) by generating SQL scripts that update the Pegasus database.  In order for that to work, however, Pegasus database needs to be used to generate input files that the present software uses.  I choose to omit further detail at this point.

### Multilingual Support
This software has only been used with English, but it should work with other languages as well.


## Dependencies
- A BSD system (e.g., FreeBSD or MacOS)
- The `parallel` shell utility (e.g., [FreeBSD](https://www.freshports.org/sysutils/parallel), [MacOS](http://brewformulas.org/Parallel); optional)
- [Google Ngram data](http://storage.googleapis.com/books/ngrams/books/datasetsv2.html) (downloaded automatically)


## Usage Example
### Problem Description
This example uses only one of the Google Ngram files, `googlebooks-eng-all-3gram-20120701-aq.gz` ([direct download link](http://storage.googleapis.com/books/ngrams/books/googlebooks-eng-all-3gram-20120701-aq.gz)).  This file serves the purpose well because it is fairly small being only 82MB compressed (and 652MB uncompressed).  The `work` directory contains the sample input file we will be processing here, [`bin-fs/work/eng-3-aq-in`](bin-fs/work/eng-3-aq-in), which contains the following lines (I replaced tabs with spaces for better alignment):
```
Aquatic biota that    100
Aquatic biota which   200
Aquiclude , and       300
```
These lines have been chosen to demonstrate certain aspects of the process.  The task we face is to compute word predictability of the last word in each of the three tri-grams (i.e., words `that`, `which`, and `and`).

### Computing Word Predictability
Assuming you are starting from scratch, clone this repository and run the example with verbosity on:
```sh
git clone https://gitlab.com/ubiq-x/ngram-word-pred
cd ngram-word-pred/bin-fs
./do-word-pred proc eng 3 aq 1
```
You should get the following output (minus the annotations on the right):
```
Processing input file 'eng-3-aq-in'
    Downloading the n-grams file...                # Stage 1
    Excising data                                  # Stage 2
        From: 82M                                  #     Size of Google Ngram file
        To  : 37K  (.0435298100%)                  #     Size of the relevant part of it
        Time: 00:00:46
    Computing word predictability                  # Stage 3
        N-grams: 3                                 #     Number of input n-grams
            Aquatic biota that;  preserve case
                n_a : 140
                n_b : 0
                pred: 0
            Aquatic biota that;  ignore case
                n_a : 3625                         #     count(context)
                n_b : 61                           #     count(context + word)
                pred: .0168275862                  #     n_b / b_a
            Aquatic biota which;  preserve case
                n_a : 140;  cached                 #     Value cached
                n_b : 0
                pred: 0
            Aquatic biota which;  ignore case      #     Ignore case when matching
                n_a : 3625;  cached
                n_b : 0
                pred: 0
            Aquiclude , and;  preserve case        #     Preserve case when matching
                n_a :                              #     count(context) = 0
                n_b : 0
                pred: -1
            Aquiclude , and;  ignore case
                n_a : 72
                n_b : 72
                pred: 1.0000000000                 #     A n-gram unique to the dataset
        Time: 00:00:00
Done
```
The process happens in three stages: (1) Downloading the required Google Ngram file (if it doesn't exist already), (2) excising from it only parts relevant to the input n-grams, and (3) computing word predictability for all input n-grams.

Here a several things to note about the above output:
- Each input n-gram is matched in a case-sensitive and a case-insensitive manner.
- Several of the predictabilities are `0` and that is evidently due to the `count(context + word)` (i.e., `n_b`) being zero.
- One of the predictabilities is `-1` as a result of `count(context)` (i.e., `n_a`) being zero.
- The last of the predictabilities is `1` indicating that both the entire corresponding n-gram (i.e., `Aquiclude , and`) and its context (i.e., `Aquiclude ,`) is unique to the Google Ngram dataset.
- Google Ngram dataset treats punctuation as separate tokens.  In reading research, punctuation is typically assumed to be part of the preceding word.  Just something to keep in mind.
- Two values were cached from prior computations.  This sort of caching only affects adjacent input n-grams and is the reason why the input file should be alphabetically sorted.
- Edge cases where predictability is either `0` or `1` should be inspected carefully and either excluded from further analysis or changed to less extreme values because they are probably artifacts of the sampling process employed by Google (no offense, Google).

Here is how the generated output file `end-3-aq-out` should look like (like before, I replaced tabs with spaces for better alignment):
```
Aquatic biota that    100   cp   0
Aquatic biota that    100   ci   .0168275862
Aquatic biota which   200   cp   0
Aquatic biota which   200   ci   0
Aquiclude , and       300   cp   -1
Aquiclude , and       300   ci   1.0000000000
```
If you compare the input and output files, you will notice that the second column (both files are tab-delimited) is simply copied from the input file into the output file.  There is more to that, in fact.  Namely, all lines in the output file are copies of the corresponding lines from the input file with two extra columns added at the end: Case sensitivity (`cp` for case preserve or `ci` for case ignore) and word predictability.  You should use this behavior to your advantage.  For example, if you are planning on using n-grams from different books in one computational batch to save on computation time (a wise choice indeed), use as many extra columns as you need to properly distinguish between n-grams from different books, different chapters within each book, etc.

If you were to run the original command to compute word predictability again right now, you would discover that the computation takes much shorter than before.  That's because of memoization; all completed computations are cached and used in all runs following the initial one.  This time the output should look like this:
```
Processing input file 'eng-3-aq-in'
    Data file exists
        From: 82M
        To  : 37K  (.0435298100%)
    Computing word predictability
        N-grams: 3
            Aquatic biota that;  preserve case
                pred: 0;  cached
            Aquatic biota that;  ignore case
                pred: .0168275862;  cached
            Aquatic biota which;  preserve case
                pred: 0;  cached
            Aquatic biota which;  ignore case
                pred: 0;  cached
            Aquiclude , and;  preserve case
                pred: -1;  cached
            Aquiclude , and;  ignore case
                pred: 1.0000000000;  cached
        Time: 00:00:00
Done
```
Going back all the way to the beginning of this example, using verbose output is good for debugging but impractical for actual computations.  Fortunately it can be easily turned off:
```sh
./do-word-pred proc eng 3 aq 0
```
Of course, all input files can be processed with one command:
```sh
./do-word-pred proc-all 0
```
Additionally, they can even be processed in parallel using as many processes as you want (here, 4):
```sh
./do-word-pred proc-all-par 4
```
Of course, to get that working you will need to install `parallel` shell utility for your [FreeBSD](https://www.freshports.org/sysutils/parallel) or [MacOS](http://brewformulas.org/Parallel) system.


## Setup
No setup is required; cloning this repository is enough.  Still, before using this software I recommend reading at least the following earlier sections: [Input and Output Files](#input-and-output-files), [Customizing](#customizing), [Pushover Notifications](#pushover-notifications), and [Usage](#usage).


## Command Reference
Below are all the commands the [`bin-fs/do-word-pred`](bin-fs/do-word-pred) shell script executes.

- **`del-all <lang> <n> <letters>`**  
  Delete the excised data file and all processing results.  The data file does not refer to the Google Ngram data file but rather the data file that has been built based upon it to solve the relevant input file.

- **`del-data <lang> <n> <letters>`**  
  Delete the excised data file.  The data file does not refer to the Google Ngram data file but rather the data file that has been built based upon it to solve the relevant input file.

- **`del-res <lang> <n> <letters>`**  
  Delete all processing results (including all memoized results).  This command is useful if a complete re-processing is desired, e.g., after updating the input file or deleting a corrupt Google Ngram data file.

- **`download <lang> <n> <letters> <force?>`**  
  Download one Google Ngram file.  Providing `1` for `<force?>` re-downloads the specified file if it already exists; if `0` is used, download is skipped.

- **`download-all <force?>`**  
  Download all Google Ngram files.  Modify the script to select the files to download.  Providing `1` for `<force?>` re-downloads existing files; if `0` is used, existing files are skipped.

- **`gen-pegasus-sql <lang> <n> <letters> <append-to-file>`**  
  Generate [Pegasus](http://gitlab.com/ubiq-x/pegasus) SQL script.  This is unlikely to be useful to anyone at this point.

- **`gen-pegasus-sql-all <append-to-file>`**  
  Generate all [Pegasus](http://gitlab.com/ubiq-x/pegasus) SQL scripts.  This is unlikely to be useful to anyone at this point.

- **`proc <lang> <n> <letters> <is-verbose?>`**  
  Process one input file.

- **`proc-all <is-verbose?>`**  
  Process all input files.

- **`proc-all-par <n-proc>`**  
  Process all input files in parallel.


## Acknowledgments
- This software uses [Google Ngram data](http://storage.googleapis.com/books/ngrams/books/datasetsv2.html).
- I worked as a post-doctoral research associate at the University of Pittsburgh when I developed this software.


## References
Lin Y., Michel J.-B., Aiden E.L., Orwant J., Brockman W., & Petrov S. (2012) Syntactic Annotations for the Google Books Ngram Corpus. _Proceedings of the 50th Annual Meeting of the Association for Computational Linguistics Volume 2: Demo Papers (ACL'12)_.

Michel J.-B., Shen Y.K., Aiden A.P., Veres A., Gray M.K., Brockman W., The Google Books Team, Pickett J.P., Hoiberg D., Clancy D., Norvig P., Orwant J., Pinker S., Nowak M.A., & Aiden E.L. (2011) Quantitative Analysis of Culture Using Millions of Digitized Books.  _Science, 331(6014)_, 176-182.

Taylor, W.L. (1953) "Cloze Procedure": A New Tool for Measuring Readability.  _Journalism Quarterly, 30_, 415–433.


## Citing
Loboda, T.D. (2015).  N-gram Word Predictability [computer software].  Available at https://gitlab.com/ubiq-x/ngram-word-pred


## License
This project is licensed under the [BSD License](LICENSE.md).
