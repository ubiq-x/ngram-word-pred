-- This SQL script should not be executed directly. The Ngram app will read it and before executing 
-- substitute the "__schema__" string with the schema name corresponding to the language

--DROP DATABASE IF EXISTS "ngram";
--CREATE DATABASE "ngram" WITH ENCODING 'UTF8' TEMPLATE=template0;

--\c ngram


BEGIN;

CREATE SCHEMA "__schema__";
SET search_path TO "__schema__";


----------------------------------------------------------------------------------------------------
--DROP TABLE IF EXISTS n5;
--DROP TABLE IF EXISTS n4;
--DROP TABLE IF EXISTS n3;
--DROP TABLE IF EXISTS data_file;


----------------------------------------------------------------------------------------------------
-- Reset sequences (TODO)


----------------------------------------------------------------------------------------------------
--CREATE TABLE __SCHEMA__.lang
--(
--  id BIGSERIAL PRIMARY KEY NOT NULL,
--  name char(3) NOT NULL
--);

--  lang_id bigint NOT NULL REFERENCES lang(id) ON DELETE CASCADE ON UPDATE CASCADE,


----------------------------------------------------------------------------------------------------
CREATE TABLE data_file
(
  id SMALLSERIAL PRIMARY KEY NOT NULL,  -- 1 to 32767
  n smallint NOT NULL,
  letters char(2) NOT NULL,
  n_ngram integer NOT NULL DEFAULT 0 CHECK (n_ngram >= 0),
  n_ngram_imp integer NOT NULL DEFAULT 0 CHECK (n_ngram_imp >= 0),
  year_min smallint NOT NULL DEFAULT 0 CHECK (year_min >= 0),
  year_max smallint NOT NULL DEFAULT 0 CHECK (year_max >= 0),
  is_upd smallint NOT NULL DEFAULT 0,
  imported timestamp NOT NULL DEFAULT now()
);

COMMENT ON COLUMN data_file.n IS 'The n in ngram; e.g., 3';
COMMENT ON COLUMN data_file.letters IS 'The first two letters of ngrams in the data file';
COMMENT ON COLUMN data_file.n_ngram IS 'Number of ngrams in the data file';
COMMENT ON COLUMN data_file.n_ngram_imp IS 'Number of ngrams actually imported from the data file';
COMMENT ON COLUMN data_file.year_min IS 'The earliest year ngram from which has been imported';
COMMENT ON COLUMN data_file.year_max IS 'The latest year ngram from which has been imported';
COMMENT ON COLUMN data_file.is_upd IS 'Are the statistics for this data file updated?';


----------------------------------------------------------------------------------------------------
CREATE TABLE n3
(
  w1 text NOT NULL,
  w2 text NOT NULL,
  w3 text NOT NULL,
  year smallint NOT NULL,
  n integer NOT NULL CHECK (n > 0)
  --n_vol integer NOT NULL CHECK (n_vol > 0)
);

COMMENT ON COLUMN n3.w1 IS 'Word 1';
COMMENT ON COLUMN n3.w2 IS 'Word 2';
COMMENT ON COLUMN n3.w3 IS 'Word 3';
COMMENT ON COLUMN n3.n IS 'Number of occurrences of the ngram';
--COMMENT ON COLUMN n3.n_vol IS 'Number of volumes in which the ngram occurs';

--DROP INDEX IF EXISTS n3_data_file_id;  CREATE INDEX n3_data_file_id ON data_file (id);


----------------------------------------------------------------------------------------------------
CREATE TABLE n4
(
  w1 text NOT NULL,
  w2 text NOT NULL,
  w3 text NOT NULL,
  w4 text NOT NULL,
  year smallint NOT NULL,
  n integer NOT NULL CHECK (n > 0)
);

COMMENT ON COLUMN n4.w1 IS 'Word 1';
COMMENT ON COLUMN n4.w2 IS 'Word 2';
COMMENT ON COLUMN n4.w3 IS 'Word 3';
COMMENT ON COLUMN n4.w4 IS 'Word 4';
COMMENT ON COLUMN n4.n IS 'Number of occurrences of the ngram';


----------------------------------------------------------------------------------------------------
CREATE TABLE n5
(
  w1 text NOT NULL,
  w2 text NOT NULL,
  w3 text NOT NULL,
  w4 text NOT NULL,
  w5 text NOT NULL,
  year smallint NOT NULL,
  n integer NOT NULL CHECK (n > 0)
);

COMMENT ON COLUMN n5.w1 IS 'Word 1';
COMMENT ON COLUMN n5.w2 IS 'Word 2';
COMMENT ON COLUMN n5.w3 IS 'Word 3';
COMMENT ON COLUMN n5.w4 IS 'Word 4';
COMMENT ON COLUMN n5.w5 IS 'Word 5';
COMMENT ON COLUMN n5.n IS 'Number of occurrences of the ngram';


----------------------------------------------------------------------------------------------------
COMMIT;
