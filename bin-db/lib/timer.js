/**
 * General purpose timers manager.
 */

var moment = require("moment");

var timers = [];


// -----------------------------------------------------------------------------------------------------------------------------------
exports.start = function(name) {
  timers[name] = Date.now();
};


// -----------------------------------------------------------------------------------------------------------------------------------
exports.end = function(name, doConvertToTime) {
  var t = timers[name];
  //if (!t) throw new Error("No such timer: " + name);
  if (!t) return -2;
  var dur = Date.now() - t;
  //if (doLog) console.log((logPre || "") + (doLogElapsed ? "Time elapsed: " : "") + (doLogName ? name + ": " : "") + dur + (doLogMs ? "ms" : "") + (logSuff || ""));
  return (doConvertToTime ? exports.msToTime(dur) : dur);
};


// -----------------------------------------------------------------------------------------------------------------------------------
exports.msToTime = function(ms, doForceDays, doForceHours) {
  var d = moment.duration(ms).days();
  var h = moment.duration(ms).hours();
  var m = moment.duration(ms).minutes();
  var s = moment.duration(ms).seconds();
  
  return ((d > 0 || doForceDays ? d + "." : "") + (h > 0 || doForceDays || doForceHours ? (h < 10 ? "0" + h : h) + ":" : "") + (m < 10 ? "0" + m : m) + ":" + (s < 10 ? "0" + s : s));
};
