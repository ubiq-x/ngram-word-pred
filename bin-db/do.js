/*
BSD 3-Clause License

Copyright (c) 2015, Tomek D. Loboda
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. */


/* =====================================================================================================================
 *
 * This command line program manages the NGram application.  The following node.js modules need to be
 * installed for this program to run:
 *
 *   npm install fs, http, os, moment, path, pg, pushover-notifications, readline, request, shelljs, text-table, url
 *
 * Currently unused packages:
 *
 *   npm install line-by-lin, string.prototype.endswith, status-bar
 *
 * ================================================================================================================== */




// ----[ CONST ]--------------------------------------------------------------------------------------------------------

var DIR_LIB               = process.cwd() + "/lib",
    DIR_DATA              = process.cwd() + "../data",
    DIR_LOG               = process.cwd() + "/log",
    DIR_WORK              = process.cwd() + "/work",
    PRIVATE               = require(DIR_LIB + "/private.js"),
    PUSHOVER_USR_KEY      = PRIVATE.PUSHOVER_USR_KEY || "",
    PUSHOVER_APP_KEY      = PRIVATE.PUSHOVER_APP_KEY || "",
    PUSHOVER_DEV          = PRIVATE.PUSHOVER_DEV     || "",
    PUSHOVER_SND          = PRIVATE.PUSHOVER_SND     || "",
    PUSHOVER_HOST         = PRIVATE.PUSHOVER_HOST    || "",
    FILE_SUFF_DL          = ".part",  // filename suffix to add to a file currently being downloaded
    DB_VALUES_MAX_CNT     = 1000,     // how many values to accumulate before executing an insert query
    SERVER_PORT           = 10000,
    LANG                  = [ "eng", "eng-us", "eng-gb", "eng-fiction", "spa" ],  // supported languages
    URL_GOOGLE_NGRAM_HOST = "storage.googleapis.com",
    URL_GOOGLE_NGRAM_PATH = "/books/ngrams/books/",
    POS                   = [ "NOUN", "VERB", "ADJ", "ADV", "PRON", "DET", "ADP", "NUM", "CONJ", "PRT", "X", "." ],  // part-of-speech tags
    LETTERS               = [
      //"0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
      "_ADJ_", "_ADP_", "_ADV_", "_CONJ_", "_DET_", "_NOUN_", "_NUM_", "_PRON_", "_PRT_", "_VERB_",
      "a_", "aa", "ab", "ac", "ad", "ae", "af", "ag", "ah", "ai", "aj", "ak", "al", "am", "an", "ao", "ap", "aq", "ar", "as", "at", "au", "av", "aw", "ax", "ay", "az",
      "b_", "ba", "bb", "bc", "bd", "be", "bf", "bg", "bh", "bi", "bj", "bk", "bl", "bm", "bn", "bo", "bp", "bq", "br", "bs", "bt", "bu", "bv", "bw", "bx", "by", "bz",
      "c_", "ca", "cb", "cc", "cd", "ce", "cf", "cg", "ch", "ci", "cj", "ck", "cl", "cm", "cn", "co", "cp", "cq", "cr", "cs", "ct", "cu", "cv", "cw", "cx", "cy", "cz",
      "d_", "da", "db", "dc", "dd", "de", "df", "dg", "dh", "di", "dj", "dk", "dl", "dm", "dn", "do", "dp", "dq", "dr", "ds", "dt", "du", "dv", "dw", "dx", "dy", "dz",
      "e_", "ea", "eb", "ec", "ed", "ee", "ef", "eg", "eh", "ei", "ej", "ek", "el", "em", "en", "eo", "ep", "eq", "er", "es", "et", "eu", "ev", "ew", "ex", "ey", "ez",
      "f_", "fa", "fb", "fc", "fd", "fe", "ff", "fg", "fh", "fi", "fj", "fk", "fl", "fm", "fn", "fo", "fp", "fq", "fr", "fs", "ft", "fu", "fv", "fw", "fx", "fy", "fz",
      "g_", "ga", "gb", "gc", "gd", "ge", "gf", "gg", "gh", "gi", "gj", "gk", "gl", "gm", "gn", "go", "gp", "gq", "gr", "gs", "gt", "gu", "gv", "gw", "gx", "gy", "gz",
      "h_", "ha", "hb", "hc", "hd", "he", "hf", "hg", "hh", "hi", "hj", "hk", "hl", "hm", "hn", "ho", "hp", "hq", "hr", "hs", "ht", "hu", "hv", "hw", "hx", "hy", "hz",
      "i_", "ia", "ib", "ic", "id", "ie", "if", "ig", "ih", "ii", "ij", "ik", "il", "im", "in", "io", "ip", "iq", "ir", "is", "it", "iu", "iv", "iw", "ix", "iy", "iz",
      "j_", "ja", "jb", "jc", "jd", "je", "jf", "jg", "jh", "ji", "jj", "jk", "jl", "jm", "jn", "jo", "jp", "jq", "jr", "js", "jt", "ju", "jv", "jw", "jx", "jy", "jz",
      "k_", "ka", "kb", "kc", "kd", "ke", "kf", "kg", "kh", "ki", "kj", "kk", "kl", "km", "kn", "ko", "kp", "kq", "kr", "ks", "kt", "ku", "kv", "kw", "kx", "ky", "kz",
      "l_", "la", "lb", "lc", "ld", "le", "lf", "lg", "lh", "li", "lj", "lk", "ll", "lm", "ln", "lo", "lp", "lq", "lr", "ls", "lt", "lu", "lv", "lw", "lx", "ly", "lz",
      "m_", "ma", "mb", "mc", "md", "me", "mf", "mg", "mh", "mi", "mj", "mk", "ml", "mm", "mn", "mo", "mp", "mq", "mr", "ms", "mt", "mu", "mv", "mw", "mx", "my", "mz",
      "n_", "na", "nb", "nc", "nd", "ne", "nf", "ng", "nh", "ni", "nj", "nk", "nl", "nm", "nn", "no", "np", "nq", "nr", "ns", "nt", "nu", "nv", "nw", "nx", "ny", "nz",
      "o_", "oa", "ob", "oc", "od", "oe", "of", "og", "oh", "oi", "oj", "ok", "ol", "om", "on", "oo", "op", "oq", "or", "os", "ot", "ou", "ov", "ow", "ox", "oy", "oz",
      "p_", "pa", "pb", "pc", "pd", "pe", "pf", "pg", "ph", "pi", "pj", "pk", "pl", "pm", "pn", "po", "pp", "pq", "pr", "ps", "pt", "pu", "pv", "pw", "px", "py", "pz",
      "q_", "qa", "qb", "qc", "qd", "qe", "qf", "qg", "qh", "qi", "qj", "qk", "ql", "qm", "qn", "qo", "qp", "qq", "qr", "qs", "qt", "qu", "qv", "qw", "qx", "qy", "qz",
      "r_", "ra", "rb", "rc", "rd", "re", "rf", "rg", "rh", "ri", "rj", "rk", "rl", "rm", "rn", "ro", "rp", "rq", "rr", "rs", "rt", "ru", "rv", "rw", "rx", "ry", "rz",
      "s_", "sa", "sb", "sc", "sd", "se", "sf", "sg", "sh", "si", "sj", "sk", "sl", "sm", "sn", "so", "sp", "sq", "sr", "ss", "st", "su", "sv", "sw", "sx", "sy", "sz",
      "t_", "ta", "tb", "tc", "td", "te", "tf", "tg", "th", "ti", "tj", "tk", "tl", "tm", "tn", "to", "tp", "tq", "tr", "ts", "tt", "tu", "tv", "tw", "tx", "ty", "tz",
      "u_", "ua", "ub", "uc", "ud", "ue", "uf", "ug", "uh", "ui", "uj", "uk", "ul", "um", "un", "uo", "up", "uq", "ur", "us", "ut", "uu", "uv", "uw", "ux", "uy", "uz",
      "v_", "va", "vb", "vc", "vd", "ve", "vf", "vg", "vh", "vi", "vj", "vk", "vl", "vm", "vn", "vo", "vp", "vq", "vr", "vs", "vt", "vu", "vv", "vw", "vx", "vy", "vz",
      "w_", "wa", "wb", "wc", "wd", "we", "wf", "wg", "wh", "wi", "wj", "wk", "wl", "wm", "wn", "wo", "wp", "wq", "wr", "ws", "wt", "wu", "wv", "ww", "wx", "wy", "wz",
      "x_", "xa", "xb", "xc", "xd", "xe", "xf", "xg", "xh", "xi", "xj", "xk", "xl", "xm", "xn", "xo", "xp", "xq", "xr", "xs", "xt", "xu", "xv", "xw", "xx", "xy", "xz",
      "y_", "ya", "yb", "yc", "yd", "ye", "yf", "yg", "yh", "yi", "yj", "yk", "yl", "ym", "yn", "yo", "yp", "yq", "yr", "ys", "yt", "yu", "yv", "yw", "yx", "yy", "yz",
      "z_", "za", "zb", "zc", "zd", "ze", "zf", "zg", "zh", "zi", "zj", "zk", "zl", "zm", "zn", "zo", "zp", "zq", "zr", "zs", "zt", "zu", "zv", "zw", "zx", "zy", "zz",
      "other", "punctuation"
    ];
      // The first two letters which is how Google has divided its ngram corpus into separate files. Note, that some of the entries
      // in this array are commented away.  That is because currently I'm not interested in importing all the data.

// DEBUG (comment out for production):
// LETTERS = [ "nc", "ng", "nq" ];
// LETTERS = [ "aa", "a_", "ab" ];




// ----[ LIB ]----------------------------------------------------------------------------------------------------------

//require("string.prototype.endswith");

var fs         = require("fs"),
    http       = require("http"),
    moment     = require("moment"),
    os         = require("os"),
    path       = require("path"),
    push       = require("pushover-notifications"),
    readline   = require("readline"),
    request    = require("request"),
    sh         = require("shelljs"),
    tt         = require("text-table"),
    url        = require("url"),

    dbi        = require(DIR_LIB + "/dbi.js"),
    timer      = require(DIR_LIB + "/timer.js");

    //statusBar  = require("status-bar")
    //linebyline = require("line-by-line")
    //zlib       = require("zlib")




// ----[ VAR ]----------------------------------------------------------------------------------------------------------

var progName = path.basename(process.argv[1]);  // the name of this very file (in case it chances)

var hasSQLErrorOccured = false;
  // I use this global variable for ease of debugging -- I break program execution right after an SQL error happens to
  // easily inspect the offending query

var isMSQueryRunning = false;  // is a query against the Microsoft Ngram service running?




// ----[ NODE ]---------------------------------------------------------------------------------------------------------

process.maxTickDepth = 10000;  // without this error will be thrown due to asynchronous nature of the "line-by-line" library




// ----[ CODE ]---------------------------------------------------------------------------------------------------------

function dataGetSizeAll(lang) {
  dataGetSizeOne(lang, 3, 0, 0, function (sizeSum) { console.log("Total: " + utilBytesToSize(sizeSum)); });
}


// ---------------------------------------------------------------------------------------------------------------------
function dataGetSizeOne(lang, n, lettersIdx, sizeSum, fnCb) {
  var filename = "googlebooks-" + lang + "-all-" + n + "gram-20120701-" + LETTERS[lettersIdx] + ".gz";

  var fnNext = function (size) {
    if (lettersIdx === (LETTERS.length - 1) && n === 5) return fnCb(sizeSum + size);

    if (lettersIdx === (LETTERS.length - 1)) n++, lettersIdx=0;
    else lettersIdx++;

    dataGetSizeOne(lang, n, lettersIdx, (sizeSum + size), fnCb);
  };

  http.request({ host: URL_GOOGLE_NGRAM_HOST, port: 80, path: URL_GOOGLE_NGRAM_PATH + filename, method: "HEAD", agent: false }, function (res) {
    var size = parseInt(res.headers["content-length"]);
    console.log(filename + ": " + utilBytesToSize(size));
    fnNext(size);
  }).on("error", function(err) {
    console.error("Error.", err);
  }).end();
}


// ---------------------------------------------------------------------------------------------------------------------
/**
 * IN:
 *   doCont : keep downloading subsequent files based on the LETTERS array?
 */
function dataDownloadOne(lang, n, lettersIdx, doCont, fnCb, timeSum, sizeSum) {
  // (1) Initialize:
  if (!fs.existsSync(DIR_DATA)) fs.mkdirSync(DIR_DATA);

  var filename     = "googlebooks-" + lang + "-all-" + n + "gram-20120701-" + LETTERS[lettersIdx] + ".gz";
  var filepath     = DIR_DATA + "/" + filename;
  var filepathPart = filepath + FILE_SUFF_DL;

  var size = 0, t = 0;

  if (!timeSum) timeSum = 0;
  if (!sizeSum) sizeSum = 0;

  // (5) Rename file just downloaded, print some info, and initiate download of the next file if necessary:
  var fnNext = function (doRename) {
    if (doRename) fs.renameSync(filepathPart, filepath);

    if (lettersIdx === (LETTERS.length - 1) && n === 5) return (fnCb ? fnCb(timeSum + t, sizeSum + size) : null);

    if (lettersIdx === (LETTERS.length - 1)) n++, lettersIdx=0;
    else lettersIdx++;

    if (doCont) dataDownloadOne(lang, n, lettersIdx, doCont, fnCb, timeSum + t, sizeSum + size);
    else if (fnCb) fnCb(timeSum + t, sizeSum + size);
  };

  // (2) Skip file if it exists already:
  if (fs.existsSync(filepath)) {
    process.stdout.write("File exists: " + filename + " (" + utilGetFileSize(filepath, true) + ")\n");
    return fnNext(false);
  }

  // (3) Remove partially downoaded file:
  if (fs.existsSync(filepathPart)) fs.unlinkSync(filepathPart);

  // (4) Download file:
  timer.start("dataDownloadOne");
  process.stdout.write("Downloading file: " + filename + "... ");
  request("http://" + URL_GOOGLE_NGRAM_HOST + URL_GOOGLE_NGRAM_PATH + filename).pipe(fs.createWriteStream(filepathPart))  // request(...).pipe(zlib.createGunzip()).pipe(out)
    .on("finish", function () {
      size = utilGetFileSize(filepathPart, false);
      t    = timer.end("dataDownloadOne");

      process.stdout.write("Done (" + utilBytesToSize(size) + ", " + timer.msToTime(t) + ", " + t + "ms)\n");

      log("download.log", true, [ filename, timer.msToTime(t, false, true), utilBytesToSize(size), t, size ]);

      pushover("Download complete", lang + "." + n + "." + LETTERS[lettersIdx] + " (" + utilBytesToSize(size) + ", " + timer.msToTime(t) + ")", -1);

      fnNext(true);
    });
}


// ---------------------------------------------------------------------------------------------------------------------
/**
 * This function initiates file downloaded if it can't be found in the data directory.
 *
 * IN:
 *   doCont : keep importing subsequent files based on the LETTERS array?
 *
 * RES:
 *   www.thegeekstuff.com/2010/02/awk-conditional-statements/
 *   www.grymoire.com/Unix/Sed.html
 */
function dataImportOne(dbc, lang, n, lettersIdx, doExcNum, doExcPunct, doExcAllCaps, doExcAllPOS, doRemoveData, doCont, fnCb, timeSum, sizeSum, nLineSum, nLineImpSum) {
  // (1) Initialize:
  if (!fs.existsSync(DIR_WORK)) fs.mkdirSync(DIR_WORK);

  if (hasSQLErrorOccured) return;

  var t = 0, size = 0;

  if (!timeSum) timeSum = 0;
  if (!sizeSum) sizeSum = 0;

  if (!nLineSum)    nLineSum    = 0;
  if (!nLineImpSum) nLineImpSum = 0;

  var filename = "googlebooks-" + lang + "-all-" + n + "gram-20120701-" + LETTERS[lettersIdx];
  var tblName = "n" + n + "_" + LETTERS[lettersIdx];

  // (4) Initiate processing of the next data file (if necessary):
  /*
  var fnNext = function () {
    if (lettersIdx === (LETTERS.length - 1) && n === 5) return (fnCb ? fnCb((nLinesSum + nLines), (nNgramsSum + nNgrams)) : null);

    if (lettersIdx === (LETTERS.length - 1)) n++, lettersIdx=0;
    else lettersIdx++;

    if (doCont) dataImportOne(dbc, lang, n, lettersIdx, doExcNum, doExcPunct, doExcAllCaps, doExcAllPOS, (nLinesSum + nLines), (nNgramsSum + nNgrams), doCont, fnCb);
    else if (fnCb) fnCb((nLinesSum + nLines), (nNgramsSum + nNgrams));
  };
  */

  // (3) Uncompress, process, and import data:
  var fnProc02 = function (dataFileId) {
    process.stdout.write("    Uncompressing file, processing it, and importing data... ");
    timer.start("dataImportOne.process");

    var awkFields = [], awkGSub = [], grepRegExp = [];
    for (var i = 0; i < 2+n; i++) {
      awkFields.push("$" + (i+1));
      awkGSub.push("gsub(/\\\\/,\"\\\\\\\\\",$" + (i+1) + ")");
    }

    if (doExcNum) grepRegExp.push("((.* \\d+(_NUM| ).*|^\\d+(_NUM| ).*|.* \\d+(_NUM)*)\\t\\d+\\t\\d+\\t\\d+)");

    //var out = sh.exec((os.platform() === "freebsd" ? "zcat " : "gzcat ") + DIR_DATA + "/" + filename + ".gz | " + (grepRegExp.length === 0 ? "" : "grep -Ev \"" + grepRegExp.join("|") + "\" | ") + " awk 'BEGIN {FS=\"[ ]+|[\\t]+\";OFS=\"\\t\";}{" + awkGSub.join(";") + "; print \"" + dataFileId + "\"," + awkFields.join(",") + ";}' | psql -U " + (os.platform() === "freebsd" ? "pgsql" : "postgres") + " -h localhost -d ngram -c \"COPY " + lang + "." + tblName + " FROM stdin;\"", { silent: false }).output;
    var out = sh.exec((os.platform() === "freebsd" ? "zcat " : "gzcat ") + DIR_DATA + "/" + filename + ".gz | " + (grepRegExp.length === 0 ? "" : "grep -Ev \"" + grepRegExp.join("|") + "\" | ") + " awk 'BEGIN {FS=\"[ ]+|[\\t]+\";OFS=\"\\t\";}{" + awkGSub.join(";") + "; print " + awkFields.join(",") + ";}' | psql -U " + (os.platform() === "freebsd" ? "pgsql" : "postgres") + " -h localhost -d ngram -c \"COPY " + lang + "." + tblName + " FROM stdin;\"", { silent: false }).output;

    //^[a-zA-Z][a-z]*[']{0,1}[a-z]+( |_([A-Z]+|[.]) )$
    //^.*(.)\1{2,}.*$

    /*
    var grepRegExpYes = "^"
    var grepRegExpNo  = "^(?i)";
    for (var i = 0; i < n; i++) {
      var sep = (i === n-1 ? "\t" : " ");
      grepRegExpYes += "([a-zA-Z][a-z]*[']{0,1}[a-z]+(" + sep + "|[.]" + sep + "|_([A-Z]+|[.])" + sep + ")|_(NOUN|VERB|ADJ|ADV|PRON|DET|ADP|NUM|CONJ|PRT|X|.)_" + sep + "|[.,;-()]|s|'s|'ll)";
      grepRegExpNo += ".*(.)\1{2,}.*" + sep;
    }
    grepRegExpYes += ".+$";
    grepRegExpNo  += ".+$";

    // sort -k 1
    var out = sh.exec((os.platform() === "freebsd" ? "zcat " : "gzcat ") + DIR_DATA + "/" + filename + ".gz | grep -E \"" + grepRegExpYes + "\" | grep -Ev \"" + grepRegExpNo + "\" | awk 'BEGIN {FS=\"[ ]+|[\\t]+\";OFS=\"\\t\";}{" + awkGSub.join(";") + "; print " + awkFields.join(",") + ";}' | psql -U " + (os.platform() === "freebsd" ? "pgsql" : "postgres") + " -h localhost -d ngram -c \"COPY " + lang + "." + tblName + " FROM stdin;\"", { silent: false }).output;
    */

    if (out && out.length > 0) console.log(out);

    //zcat /usr/home/tomek/prj/ngram/data/googlebooks-eng-all-3gram-20120701-bv.gz | grep -Ev "((.* \d+(_NUM| ).*|^\d+(_NUM| ).*|.* \d+(_NUM)*)\t\d+\t\d+\t\d+)" |  awk 'BEGIN {FS="[ ]+|[\t]+";OFS="\t";}{gsub(/\\/,"\\\\",$1);gsub(/\\/,"\\\\",$2);gsub(/\\/,"\\\\",$3);gsub(/\\/,"\\\\",$4);gsub(/\\/,"\\\\",$5); print "2",$1,$2,$3,$4,$5;}' | psql -U pgsql -h localhost -d ngram -c "COPY eng.n3_bv FROM stdin;"
    //zcat /usr/home/tomek/prj/ngram/data/googlebooks-eng-all-3gram-20120701-bv.gz | awk 'BEGIN {FS="[ ]+|[\t]+";OFS="\t";}{gsub(/\\/,"\\\\",$1);gsub(/\\/,"\\\\",$2);gsub(/\\/,"\\\\",$3);gsub(/\\/,"\\\\",$4);gsub(/\\/,"\\\\",$5); print "2",$1,$2,$3,$4,$5;}' | psql -U pgsql -h localhost -d ngram -c "COPY eng.n3_bv FROM stdin;"

    /*
    var out = sh.exec((os.platform() === "freebsd" ? "zcat " : "gzcat ") + DIR_DATA + "/" + filename + ".gz | " + (grepRegExp.length === 0 ? "" : "grep -Ev \"" + grepRegExp.join("|") + "\" | ") + " awk 'BEGIN {FS=\"[ ]+|[\\t]+\";OFS=\"\\t\";}{" + awkGSub.join(";") + "; print \"" + dataFileId + "\"," + awkFields.join(",") + ";}' >> " + DIR_WORK + "/" + filename, { silent: false }).output;
    if (out && out.length > 0) console.log(out);
    var out = sh.exec("psql -U " + (os.platform() === "freebsd" ? "pgsql" : "postgres") + " -h localhost -d ngram -c \"COPY \\\"" + lang + "\\\"." + tblName + " FROM stdin;\"", { silent: false }).output;
    if (out && out.length > 0) console.log(out);
    */

    size = utilGetFileSize(DIR_DATA + "/" + filename + ".gz", false);
    t    = timer.end("dataImportOne.process");

    dbi.execQry(dbc, "UPDATE data_file SET imported = now(), is_upd = 0 WHERE id = " + dataFileId + ";", function (err, res) {
      process.stdout.write("Done (" + utilBytesToSize(size) + ", " + timer.msToTime(t) + ", " + t + "ms)\n");

      log("import.log", true, [ filename, timer.msToTime(t, false, true), utilBytesToSize(size), t, size ]);

      pushover("Import complete", lang + "." + n + "." + LETTERS[lettersIdx] + " (" + utilBytesToSize(size) + ", " + timer.msToTime(t, false) + ")", -1);

      if (doCont && lettersIdx < (LETTERS.length - 1)) dataImportOne(dbc, lang, n, lettersIdx + 1, doExcNum, doExcPunct, doExcAllCaps, doExcAllPOS, doCont, fnCb, timeSum + t, sizeSum + size, nLineSum + 0, nLineImpSum + 0);
      else if (fnCb) fnCb(timeSum + t, sizeSum + size, nLineSum + 0, nLineImpSum + 0);
    });
  };

  var fnProc01 = function (dataFileId) {
    if (doRemoveData) {
      process.stdout.write("    Removing existing data... ");
      timer.start("dataImportOne.remove");
      //dbi.execQry(dbc, "DELETE FROM n" + n + " n USING data_file df WHERE n.data_file_id = df.id AND df.n = " + n + " AND df.letters = '" + LETTERS[lettersIdx] + "';", function (err, res) {
      //dbi.execQry(dbc, "DELETE FROM n" + n + " WHERE data_file_id = " + dataFileId + ";", function (err, res) {
      dbi.execQry(dbc, "TRUNCATE ONLY " + tblName + " RESTART IDENTITY;", function (err, res) {
        process.stdout.write("Done (" + timer.end("dataImportOne.remove", true) + ", " + timer.end("dataImportOne.remove") + "ms)\n");
        fnProc02(dataFileId);
      });
    }
    else fnProc02(dataFileId);
  };

  // (2) Begin execution:
  dataDownloadOne(lang, n, lettersIdx, false, function () {
    dbi.getId(dbc, "SELECT id FROM data_file WHERE n = " + n + " AND letters = '" + LETTERS[lettersIdx] + "';", "INSERT INTO data_file (n, letters) VALUES (" + n + ", '" + LETTERS[lettersIdx] + "');", fnProc01);
  });
}


// ----^----
/**
 * This function has proven to be too slow when dealing with the enourmous files that Google ngram data set contains
 * and has been retained here because it may be useuful smaller datasets.
 *
 * TODO: If this function is to be used before it should be read carefuly and integrated into the 'dataImportOne'
 * properly.
 *
 * [funcion currently unused]
 */
function dataImportOne_lineByLine(dbc, n, filename, dataFileId, fnCb) {
  // (1) Initialize:
  var values = [];
  var nLines = 0, nNgrams = 0, nLinesMax = 0;
  var yearMin = 0, yearMax = 0;
  var linePropStep = 0;       // the proportion of the total number of line in the file that one line is (e.g., 0.000000184)
  var linePropCurr = 0;       // the current line being processed expressed as the proportion of the total number of lines in the file (e.g., 0.32)
  var linePropNextRep = 0.1;  // the proportion of the data file at which the next progress report should be printed (e.g., 0.70)
  var linePropLastRepTime = 0;
  var isLRDone = false;       // flag introduced to block double call of the 'end' event of the line-by-line object since I couldn't figure out where the hell the second calls originated from (could be error in the library)

  // (3) Finish importing and print info:
  var fnDone = function (dataFileId) {
    if (!hasSQLErrorOccured) {
      console.log("        100%");
      console.log("        Time elapsed: " + timer.end("dataImportOne.import", true) + ", " + timer.end("dataImportOne.import") + "ms");
      console.log("        Ngrams discovered: " + nLines);
      console.log("        Ngrams matching import criteria: " + nNgrams + " (" + Math.round(nNgrams / nLines * 100) + "%)");
      dbi.execQry(dbc, "UPDATE data_file SET n_ngram = " + nLines + ", n_ngram_imp = " + nNgrams + ", year_min = " + yearMin + ", year_max = " + yearMax + " WHERE id = " + dataFileId + ";", function (err, res) {
        if (err) hasSQLErrorOccured = true;
        fnCb();
      });
    }
    else fnDoNextFile();
  };

  // (2) Import:
  console.log("    Uncompressing file.");
  timer.start("dataImportOne.uncompress");

  fs.createReadStream(DIR_DATA + "/" + filename + ".gz").pipe(zlib.createGunzip()).pipe(fs.createWriteStream(DIR_WORK + "/" + filename)).on("finish", function () {
    nLinesMax = utilGetLineCnt(DIR_WORK + "/" + filename);
    linePropStep = 1 / nLinesMax;

    console.log("        Size: " + utilGetFileSize(DIR_WORK + "/" + filename, true));
    console.log("        Lines: " + nLinesMax);
    console.log("        Time elapsed: " + timer.end("dataImportOne.uncompress", true) + " (" + timer.end("dataImportOne.uncompress") + "ms)");

    // (2.1) Process the data file on a line-by-line basis importing data into the database:
    console.log("    Importing data.");
    timer.start("dataImportOne.import");
    var lr = new linebyline(DIR_WORK + "/" + filename, { skipEmptyLines: true });

    lr.on("error", function (err) {
      console.log(err);
      hasSQLErrorOccured = true;  // while SQL errors should not trigger the 'lr.onerror' event, setting this to 'true' here ensures a graceful exit and a rollback of all the DB changes so for succinctness I choose to reuse that variable
      lr.close();
    });

    lr.on("line", function (line) {
      // (2.1.1) Update state and print progress report if necessary:
      nLines++;
      linePropCurr += linePropStep;
      if (linePropCurr > linePropNextRep) {
        var t = timer.end("dataImportOne.import") - linePropLastRepTime;
        linePropLastRepTime = timer.end("dataImportOne.import");
        console.log("        " + Math.round(linePropNextRep * 100) + "% (" + t + "ms)");
        linePropNextRep = (linePropNextRep !== 0.9 ? linePropNextRep + 0.1 : 0);
      }

      if (hasSQLErrorOccured) lr.close();

      // (2.1.2) Insert accumulated ngrams into the database:
      if (values.length === DB_VALUES_MAX_CNT) {
        lr.pause();
        dataImportOne_doInsert(dbc, n, values, function () { lr.resume(); });
        values = [];
        if (hasSQLErrorOccured) lr.close();
      }

      // (2.1.3) Determine if the ngram from the current line meets import requirements:
      var fields = line.split("\t");
      var ngrams = fields[0].split(/\s+/);

      // (2.1.3.1) Exclude ngrams containinig numerals:
      if (doExcNum) {
        for (var i = 0; i < n; i++) {
          if ((/.*\d.*/).test(ngrams[i]) || ngrams[i].indexOf("_NUM", ngrams[i].length - "_NUM".length) !== -1) return;
        }
      }

      // (2.1.3.2) Exclude ngrams containinig punctuation:
      if (doExcPunct) {
        for (var i = 0; i < n; i++) {
          if ((/.*[.,;:].*/).test(ngrams[i])) return;
        }
      }

      // (2.1.3.3) Exclude ngrams containinig all-caps:
      if (doExcAllCaps) {
        for (var i = 0; i < n; i++) {
          if (!isWordAllPOS(ngrams[i]) && ngrams[i] == ngrams[i].toUpperCase()) return;
        }
      }

      // (2.1.3.4) Exclude ngrams containinig POS tags:
      if (doExcAllPOS) {
        for (var i = 0; i < n; i++) {
          if (isWordAllPOS(ngrams[i])) return;
        }
      }

      // (2.1.4) Update state and accumulate ngram (to be inserted to the database later):
      nNgrams++;

      yearMin = (yearMin === 0 ? fields[1] : Math.min(yearMin, fields[1]));
      yearMax = (yearMax === 0 ? fields[1] : Math.max(yearMax, fields[1]));

      values.push("(" + dataFileId + ",'" + dbi.escArr(ngrams).join("','") + "'," + fields[1] + "," + fields[2] + ")");
    });

    // (2.1.5) Insert any remaining accumulated ngrams into the database:
    lr.on("end", function () {
      if (isLRDone) return;
      isLRDone = true;

      if (fs.existsSync(DIR_WORK + "/" + filename)) fs.unlinkSync(DIR_WORK + "/" + filename);
      if (!hasSQLErrorOccured && values.length > 0) {
        dataImportOne_doInsert(dbc, n, values, function () { fnDone(dataFileId); });
        values = [];
      }
      else fnDone(dataFileId);
    });
  });
}


// ----^----
/**
 * [currently unused]
 */
function dataImportOne_doInsert(client, n, values, fnCb) {
  if (hasSQLErrorOccured) return;

  dbi.execQry(client, "INSERT INTO n" + n + " VALUES " + values.join(",") + ";", function (err, res) {
    hasSQLErrorOccured = !!err;
    if (fnCb) fnCb();
  });
}


// ---------------------------------------------------------------------------------------------------------------------
/**
 * http://www.postgresql.org/docs/current/interactive/ddl-partitioning.html
 *   If you are using manual VACUUM or ANALYZE commands, don't forget that you need to run them on each partition individually.
 */
function dbAnalyze(lang, n) {
  utilGetUserConfirm("Type 'analyze' to run analysis of " + n + "-grams for language '" + lang + "':", "analyze",
    function () {
      timer.start("dbAnalyze");
      var dbc = dbi.open(false);
      dbi.execQry(dbc, "ANALYZE \"" + lang + "\".n" + n + ";", function (err, res) {
        console.log("Analysis completed: " + timer.end("dbAnalyze", true) + " (" + timer.end("dbAnalyze") + "ms)");
      });

      /*
      dbAnalyze_one(dbc, lang, n, 0, function () {
        dbi.close(dbc, false, false, null, function () {
          console.log("Analysis completed: " + timer.end("dbAnalyze", true) + " (" + timer.end("dbAnalyze") + "ms)");
        });
      });
      */
    },
    function () {
      console.log("Analysis canceled.");
    }
  );
}


// ----^----
/*
function dbAnalyze_one(dbc, lang, n, lettersIdx, fnEnd) {
  timer.start("dbAnalyze_one");

  var tblName = "n" + n + "_" + LETTERS[lettersIdx];
  process.stdout.write("Table " + tblName + "... ");

  //dbi.execQry(dbc, "VACUUM ANALYZE \"" + lang + "\"." + tblName + ";", function (err, res) {
  dbi.execQry(dbc, "ANALYZE \"" + lang + "\"." + tblName + ";", function (err, res) {
    process.stdout.write(timer.end("dbAnalyze_one", true) + " (" + timer.end("dbAnalyze_one") + "ms)\n");

    if (lettersIdx < LETTERS.length-1) dbAnalyze_one(dbc, lang, n, lettersIdx+1, fnEnd);
    else if (fnEnd) fnEnd();
  });
}
*/


// ---------------------------------------------------------------------------------------------------------------------
function dbGetInf(lang) {
  dbUpdDataFileInf(lang, function () {
    timer.start("dbGetInf");

    var dbc = dbi.open(false);
    dbi.execQry(dbc,
      "SET search_path TO \"" + lang + "\";" +
      "SELECT COALESCE(SUM(n_ngram_imp), 0) AS n, COALESCE(MIN(year_min), 0) AS year_min, COALESCE(MAX(year_max), 0) AS year_max FROM data_file WHERE n = 3 UNION ALL " +
      "SELECT COALESCE(SUM(n_ngram_imp), 0) AS n, COALESCE(MIN(year_min), 0) AS year_min, COALESCE(MAX(year_max), 0) AS year_max FROM data_file WHERE n = 4 UNION ALL " +
      "SELECT COALESCE(SUM(n_ngram_imp), 0) AS n, COALESCE(MIN(year_min), 0) AS year_min, COALESCE(MAX(year_max), 0) AS year_max FROM data_file WHERE n = 5;",
      function (err, res) {
        if (!err) {
          console.log(
            tt([
              [ "Set", "Ngrams", "Years" ],
              [ "n3", res.rows[0].n, (res.rows[0].year_min == 0 ? "-" : res.rows[0].year_min + " to " + res.rows[0].year_max) ],
              [ "n4", res.rows[1].n, (res.rows[1].year_min == 0 ? "-" : res.rows[1].year_min + " to " + res.rows[1].year_max) ],
              [ "n5", res.rows[2].n, (res.rows[2].year_min == 0 ? "-" : res.rows[2].year_min + " to " + res.rows[2].year_max) ]
            ], { align: [ "l", "r", "l" ] })
          );
        }

        dbi.close(dbc, false);
        console.log("Time elapsed: " + timer.end("dbGetInf", true) + " (" + timer.end("dbGetInf") + "ms)");
      }
    );
  });
}


// ---------------------------------------------------------------------------------------------------------------------
function dbGetInfPerFile(lang) {
  return console.log("Not implemented yet.");
}


// ---------------------------------------------------------------------------------------------------------------------
/**
 * SELECT relname, relpages, (relpages*8) AS bytes FROM pg_class ORDER BY relpages DESC LIMIT 5;
 */
/*
node do.js db-init
node do.js db-populate-one eng 3 aa 1 0 0 0 0 1
node do.js db-populate-one eng 3 aj 1 0 0 0 0 1
node do.js db-get-inf eng
node do.js db-analyze eng 3
node do.js db-index-add eng 3
node do.js db-index-del eng 3
*/
function dbIndexAdd(lang, n) {
  utilGetUserConfirm("Type 'add' to add indices on all " + n + "-gram tables for language '" + lang + "':", "add",
    function () {
      timer.start("dbIndexAdd");
      var dbc = dbi.open(false);
      dbIndexAdd_one(dbc, lang, n, 0, function () {
        dbi.close(dbc, false, false, null, function () {
          console.log("Index addition completed: " + timer.end("dbIndexAdd", true) + " (" + timer.end("dbIndexAdd") + "ms)");
        });
      });
    },
    function () {
      console.log("Index addition canceled.");
    }
  );
}


// ----^----
function dbIndexAdd_one(dbc, lang, n, lettersIdx, fnEnd) {
  timer.start("dbIndexAdd_one");

  var tblName = "n" + n + "_" + LETTERS[lettersIdx];
  var idxName = tblName + "_w";

  var A = [];
  for (var i = 0; i < n-1; i++) A.push("w" + (i+1) + " text_pattern_ops");
  var sqlIdx = "CREATE INDEX " + idxName + " ON \"" + lang + "\"." + tblName + " (" + A.join(", ") + ") WITH (fillfactor = 100);"

  process.stdout.write("Table " + tblName + "... ");

  dbi.execQry(dbc, sqlIdx, function (err, res) {
    var t = timer.end("dbIndexAdd_one");
    process.stdout.write("done (" + timer.msToTime(t) + ", " + t + "ms)\n");

    dbi.execQry(dbc, "SELECT COALESCE(relpages, 0) AS relpages FROM pg_class c INNER JOIN pg_namespace n ON c.relnamespace = n.oid WHERE n.nspname = '" + lang + "' AND c.relname = '" + tblName + "';", function (err, res) {
      tblPages = (!res.rows || !res.rows[0] ? 0 : res.rows[0].relpages);
      dbi.execQry(dbc, "SELECT SUM(relpages) AS relpages FROM pg_class WHERE relname = '" + idxName + "';", function (err, res) {
        idxPages = res.rows[0].relpages;

        log("index.log", true, [ lang, n, LETTERS[lettersIdx], timer.msToTime(t, false, true), t, tblPages, (tblPages*8*1024), utilBytesToSize(tblPages*8*1024), idxPages, (idxPages*8*1024), utilBytesToSize(idxPages*8*1024) ]);

        if (lettersIdx < LETTERS.length-1) dbIndexAdd_one(dbc, lang, n, lettersIdx+1, fnEnd);
        else if (fnEnd) fnEnd();
      });
    });
  });
}


// ---------------------------------------------------------------------------------------------------------------------
function dbIndexDel(lang, n) {
  utilGetUserConfirm("Type 'del' to delete indices on all " + n + "-gram tables for language '" + lang + "':", "del",
    function () {
      timer.start("dbIndexDel");
      var dbc = dbi.open(false);
      dbIndexDel_one(dbc, lang, n, 0, function () {
        dbi.close(dbc, false, false, null, function () {
          console.log("Index deletion completed: " + timer.end("dbIndexDel", true) + " (" + timer.end("dbIndexDel") + "ms)");
        });
      });
    },
    function () {
      console.log("Index deletion canceled.");
    }
  );
}


// ----^----
function dbIndexDel_one(dbc, lang, n, lettersIdx, fnEnd) {
  timer.start("dbIndexDel_one");

  var tblName = "n" + n + "_" + LETTERS[lettersIdx];
  var idxName = tblName + "_w";

  var sqlIdx = "DROP INDEX IF EXISTS \"" + lang + "\"." + idxName + ";"

  process.stdout.write("Table " + tblName + "... ");

  dbi.execQry(dbc, sqlIdx, function (err, res) {
    var t = timer.end("dbIndexDel_one");
    process.stdout.write(timer.msToTime(t) + " (" + t + "ms)\n");

    if (lettersIdx < LETTERS.length-1) dbIndexDel_one(dbc, lang, n, lettersIdx+1, fnEnd);
    else if (fnEnd) fnEnd();
  });
}


// ---------------------------------------------------------------------------------------------------------------------
function dbInit() {
  utilGetUserConfirm("Type 'INIT' to initialize the database (if one exists already, it will be destroyed):", "init",
    function () {
      timer.start("dbInit");
      var dbc = dbi.open(false, dbi.CONN_STR_TEMPLATE1);
      dbi.execQry(dbc, "DROP DATABASE IF EXISTS \"ngram\";", function (err, res) {
        dbi.execQry(dbc, "CREATE DATABASE \"ngram\" WITH ENCODING 'UTF8' TEMPLATE=template0;", function (err, res) {
          dbi.close(dbc, false);

          dbInit_addSchema(dbi.open(false), fs.readFileSync(DIR_LIB + "/ddl.sql").toString(), 0, function (err) {
            if (err) return;

            console.log("Database initialized.");
            console.log("Time elapsed: " + timer.end("dbInit", true) + " (" + timer.end("dbInit") + "ms)");
          });
        });
      });
    },
    function () {
      console.log("Database initialization canceled.");
    }
  );
}


// ----^----
/**
 * The database is organized into schemas that correspond to languages.  This function adds such a schema and executes a DDL query to
 * create tables, indices, etc.
 *
 * Check the statistics target on a column:
 *   SELECT attstattarget FROM pg_attribute WHERE attrelid = 'eng.n3_ab'::regclass AND attname = 'w1';
 */
function dbInit_addSchema(dbc, qry, langIdx, fnCb) {
  var qryLang = qry.replace(/__SCHEMA__/gi, LANG[langIdx]);

  var qryTbl = "";
  for (var i = 3; i <= 5; i++) {
    for (var j = 0; j < LETTERS.length; j++) {
      qryTbl += "CREATE TABLE n" + i + "_" + LETTERS[j] + " () INHERITS (n" + i + ");\n";
    }

    for (var j = 1; j <= i; j++) {
      qryTbl += "ALTER TABLE eng.n" + i + "* ALTER COLUMN w" + j + " SET STATISTICS 5000;";
    }
  }

  dbi.execQry(dbc, qryLang + "\n\n" + qryTbl, function (err, res) {
    if (err) dbi.close(dbc, false, false, null, function () { if (fnCb) fnCb(err); });

    if (langIdx < LANG.length) dbInit_addSchema(dbc, qry, langIdx+1, fnCb);
    else {
      dbi.close(dbc, false);
      if (fnCb) fnCb(false);
    }
  });
}


// ---------------------------------------------------------------------------------------------------------------------
/**
 * If 'n === -1' and 'lettersIdx === -1' then it is assumed that all data files should be processed.  If either of these two
 * conditions isn't true, then a new starting point is used (e.g., 'n=4'), but all the subsequent files will be processed.
 * Furthermore, if none of these conditions is true then only one data file will be processed, the one given by the combination of
 * 'n' and 'lettersIdx' provided.
 */
function dbPopulate(lang, n, lettersIdx, doExcNum, doExcPunct, doExcAllCaps, doExcAllPOS, doRemoveData, doCont) {
  timer.start("dbPopulate");

  hasSQLErrorOccured = false;

  var dbc = dbi.open(true);
  dbi.execQry(dbc, "SET search_path TO \"" + lang + "\";", function (err, res) {
    dataImportOne(dbc, lang, n, lettersIdx, doExcNum, doExcPunct, doExcAllCaps, doExcAllPOS, doRemoveData, doCont, function (timeSum, sizeSum, nLineSum, nLineImpSum) {
      if (!hasSQLErrorOccured) {
        timer.start("dbPopulate.commit");
        dbi.close(dbc, true, true,
          function () {
            process.stdout.write("    Committing changes... ");
          },
          function (err, res) {
            process.stdout.write("Done (" + timer.end("dbPopulate.commit", true) + ", " + timer.end("dbPopulate.commit") + "ms)\n");
            //console.log("All done. In total, discovered " + nLinesSum + " and imported " + nNgramsSum + " (" + Math.round(nNgramsSum / nLinesSum * 100) + "%) ngrams.");
            process.stdout.write("Database population complete (" + utilBytesToSize(sizeSum) + ", " + timer.end("dbPopulate", true) + ", " + timer.end("dbPopulate") + "ms)\n");

            /*
            if (doMaintain) {
              timer.start("dbPopulate.maintain");
              process.stdout.write("Running database maintanance... ");
              dbc = dbi.open(false);
              dbi.execQry(dbc, "SET search_path TO \"" + lang + "\";", function (err, res) {
                dbi.execQry(dbc, "ANALYZE;", function (err, res) {
                  dbi.close(dbc, false, false, null, function () {
                    if (!err) process.stdout.write("Done (" + timer.end("dbPopulate.maintain", true) + ", " + timer.end("dbPopulate.maintain") + "ms)\n");
                    console.log("Time elapsed: " + timer.end("dbPopulate", true) + " (" + timer.end("dbPopulate") + "ms)");
                    if (doCont) pushover("Populate complete", "All done", 1);
                  });
                });
              });
            }
            else if (doCont) pushover("Populate complete", "All done", 1);
            */

            if (doCont) pushover("Populate complete", "All done", 1);
          }
        );
      }
      else {
        dbi.close(dbc, true, false, null,
          function () {
            console.log("No changes have been made to the database due to an error.");
          }
        );
      }
    });
  });
}


// ---------------------------------------------------------------------------------------------------------------------
/**
 * Refactor the databse, Phase 1: Create the data structures.
 *
 * Each original ngram table (e.g., n3_aa) will be transformed into a system of n+1 (where n=[3,4,5]) tables.  All the word columns
 * (e.g., w1, w2, etc.) will be represented as separate dictonary tables.  These tables will naturally only hold distinct and, more
 * importantly, cleaned words.  For example, strings like "A.A.A!50" are not words and will be pruned.  Once the word tables are in
 * place, the original ngram table will be recreted but IDs of the words will be used instead of actual words.  Search will be
 * performed via joins.  The word tables will be much smaller than the original ngram table word columns and thanks to that indices
 * will be smaller and faster to create and use as they will only involve word IDs (i.e., integers).  Multi-column indices on the
 * new ngram table will likely still be useful, but they will again be much more manageable than the once I attempted to create on
 * the original tables but gave up due to the time necessary and the projected resulting size (about 60% of the table size).
 *
 * While one large word table seems in order here, creating it will be problematic.  In fact, I attempted to create it, but
 * inserting words from all the original ngram tables while ensuring there are no duplicates is a very time-consuming operation
 * which alone I projected would take about two months.
 */

/*
DROP TABLE IF EXISTS eng.n3_aa_w1;  CREATE TABLE eng.n3_aa_w1 (id BIGSERIAL PRIMARY KEY NOT NULL, w TEXT NOT NULL);
DROP TABLE IF EXISTS eng.n3_aa_w2;  CREATE TABLE eng.n3_aa_w2 (id BIGSERIAL PRIMARY KEY NOT NULL, w TEXT NOT NULL);
DROP TABLE IF EXISTS eng.n3_aa_w3;  CREATE TABLE eng.n3_aa_w3 (id BIGSERIAL PRIMARY KEY NOT NULL, w TEXT NOT NULL);

INSERT INTO eng.n3_aa_w1 (w) SELECT DISTINCT w1 FROM eng.n3_aa WHERE w1 ~ $$^[a-zA-Z][a-z]*[']*[a-z]+($|_([A-Z]+|[.])$)$$ AND NOT w1 ~ '(?i)^.*(.)\1{2,}.*$';
*/

/*

"CREATE OR REPLACE FUNCTION insert_word(w_new) RETURNS void AS $$ " +
"BEGIN " +
"    INSERT INTO word (w) w_new " +
"    WHERE NOT EXISTS (SELECT 1 FROM word WHERE w = w_new);" +
//"    RETURN;" +
//"EXCEPTION WHEN unique_violation THEN" +
"END;" +
"$$ LANGUAGE plpgsql;"
;
*/
function dbRefactor01(lang) {
  timer.start("dbRefactor01");

  var dbc = dbi.open(true);

  var sql =
    "CREATE TABLE IF NOT EXISTS \"" + lang + "\".word " +
    "(" +
    "  id BIGSERIAL PRIMARY KEY NOT NULL," +
    "  w TEXT NOT NULL" +
    ");";

  dbi.execQry(dbc, sql, function (err) {
    dbi.close(dbc, true, !err, null, function () {
      console.log((!err ? "Done" : "Error occured") + " (" + timer.end("dbRefactor01", true) + ", " + timer.end("dbRefactor01") + "ms)");
    });
  });
}

// ----^----
/*
function dbRefactor01_one(dbc, lang, n, letters, idx, fnEnd) {
  if (idx === letters.length) return fnEnd(true);

  var sql =
    "CREATE TABLE IF NOT EXISTS \"" + lang + "\".n" + n + "_word_" + letters[idx] + " " +
    "(" +
    "  id BIGSERIAL PRIMARY KEY NOT NULL," +
    "  w TEXT NOT NULL" +
    ");";

  dbi.execQry(dbc, sql, function (err) {
    return (err ? fnEnd(false) : dbRefactor01_one(dbc, lang, n, letters, idx+1, fnEnd));
  });
}
*/


// ---------------------------------------------------------------------------------------------------------------------
/**
 * Refactor the databse, Phase 2: Populate the unique word tables (w1).
 */
function dbRefactor02(lang) {
  timer.start("dbRefactor02");

  var dbc = dbi.open(true);

  var fnEnd = function (isOk) {
    dbi.close(dbc, true, isOk, null, function () {
      console.log((isOk ? "Done" : "Error occured") + " (" + timer.end("dbRefactor02", true) + ", " + timer.end("dbRefactor02") + "ms)");
    });
  };

  //LETTERS = [ "aw" ];  // DEBUG
  //dbRefactor02_one(dbc, lang, n, 0, fnEnd);

  dbi.execQry(dbc, "TRUNCATE \"" + lang + "\".word;", function (err) {
    return (err ? fnEnd(false) : dbRefactor02_one(dbc, lang, 0, fnEnd));
  });
}


// ----^----
function dbRefactor02_one(dbc, lang, lettersIdx, fnEnd) {
  if (lettersIdx === LETTERS.length) return fnEnd(true);

  timer.start("dbRefactor02_one");

  var tblNameNgram = "n3_" + LETTERS[lettersIdx];

  process.stdout.write("Table " + tblNameNgram + "... ");
  dbi.execQry(dbc, "INSERT INTO \"" + lang + "\".word (w) SELECT DISTINCT w1 FROM \"" + lang + "\"." + tblNameNgram + ";", function (err) {
    var t = timer.end("dbRefactor02_one");
    process.stdout.write("done (" + timer.msToTime(t) + ", " + t + "ms)\n");
    log("refactor02.log", true, [ lang, LETTERS[lettersIdx], timer.msToTime(t, false, true), t ]);

    return (err ? fnEnd(false) : dbRefactor02_one(dbc, lang, lettersIdx+1, fnEnd));
  });
}


// ---------------------------------------------------------------------------------------------------------------------
/**
 * Refactor the databse, Phase 3: Populate the unique word tables (w2-w5).
 */
/*
node do.js db-refactor-1 eng 1
node do.js db-refactor-2 eng 1
node do.js db-refactor-3 eng 3 2 1
node do.js db-refactor-3 eng 3 3 1
*/
function dbRefactor03(lang, n, wordNum) {
  timer.start("dbRefactor03");

  var dbc = dbi.open(true);

  var fnEnd = function (isOk) {
    dbi.close(dbc, true, isOk, null, function () {
      console.log((isOk ? "Done" : "Error occured") + " (" + timer.end("dbRefactor03", true) + ", " + timer.end("dbRefactor03") + "ms)");
    });
  };

  // LETTERS = [ "aw" ];  // DEBUG
  dbRefactor03_one(dbc, lang, n, wordNum, 0, fnEnd);
}


// ----^----
function dbRefactor03_one(dbc, lang, n, wordNum, lettersIdx, fnEnd) {
  if (lettersIdx === LETTERS.length) return fnEnd(true);

  timer.start("dbRefactor03_one");

  var tblNameNgram = "n3_" + LETTERS[lettersIdx];

  //insert into test select * from temporary_table where id not in (select id from test) as a

  process.stdout.write("Table " + tblNameNgram + " (w" + wordNum + ")... ");
  //dbi.execQry(dbc, "INSERT INTO \"" + lang + "\".word (w) SELECT DISTINCT w" + wordNum + " FROM \"" + lang + "\"." + tblNameNgram + " WHERE w" + wordNum + " NOT IN (SELECT w FROM \"" + lang + "\".word);", function (err) {
  dbi.execQry(dbc, "INSERT INTO \"" + lang + "\".word (w) (SELECT DISTINCT w" + wordNum + " FROM \"" + lang + "\"." + tblNameNgram + " EXCEPT SELECT w FROM \"" + lang + "\".word);", function (err) {
    var t = timer.end("dbRefactor03_one");
    process.stdout.write("done (" + timer.msToTime(t) + ", " + t + "ms)\n");
    log("refactor03.log", true, [ lang, n, LETTERS[lettersIdx], "w" + wordNum, timer.msToTime(t, false, true), t ]);

    return (err ? fnEnd(false) : dbRefactor03_one(dbc, lang, n, wordNum, lettersIdx+1, fnEnd));
  });
}


// ---------------------------------------------------------------------------------------------------------------------
/**
 * Refactor the databse, Phase 4: Add unique constraints on the unique word tables, analyze these tables, and create indices on them.
 */
function dbRefactor04(lang, n) {
}


// ---------------------------------------------------------------------------------------------------------------------
/**
 * Refactor the databse, Phase 5: Create new ngram tables using IDs of the unique words instead of actual words.
 */
function dbRefactor05(lang, n) {
}


// ---------------------------------------------------------------------------------------------------------------------
/**
 * Refactor the databse, Phase 6: Drop the original ngram tables.
 */
function dbRefactor06(lang, n) {
}


// ---------------------------------------------------------------------------------------------------------------------
/**
 * Update all data files info.  This info is cached data on the size of each data table.
 */
function dbUpdDataFileInf(lang, fnCb) {
  // (1) Initialize:
  timer.start("dbUpdDataFileInf");
  var dbc = dbi.open(true);

  // (3) Commit the changes, print info, and return:
  var fnDone = function (doPrintInf) {
    dbi.close(dbc, true, true, null, function () {
      if (doPrintInf) process.stdout.write("    Done (" + timer.end("dbUpdDataFileInf", true) + ", " + timer.end("dbUpdDataFileInf") + "ms)\n\n");
      if (fnCb) fnCb();
    });
  };

  // (2) Update all out-of-date data files:
  dbi.execQry(dbc, "SET search_path TO \"" + lang + "\"; SELECT id, n, letters FROM data_file WHERE is_upd = 0 ORDER BY n, letters;", function (err, res) {
    if (!err && res.rows.length > 0) {
      process.stdout.write("Updating database statistics (done only once after data import)\n");
      dbUpdDataFileInf_one(dbc, res.rows, 0, function () { fnDone(true); });
    }
    else fnDone(false);
  });
}


// ----^----
function dbUpdDataFileInf_one(dbc, dataFiles, idx, fnCb) {
  timer.start("dbUpdDataFileInf_one");

  var df = dataFiles[idx];
  var tblName = "n" + df.n + "_" + df.letters;
  process.stdout.write("    Table " + tblName + "... ");

  var fnNext = function (err, res) {
    process.stdout.write(timer.end("dbUpdDataFileInf_one", true) + " (" + timer.end("dbUpdDataFileInf_one") + "ms)\n");

    if (err || (idx === dataFiles.length - 1)) return (fnCb ? fnCb() : null);
    dbUpdDataFileInf_one(dbc, dataFiles, idx+1, fnCb);
  };

  //dbi.execQry(dbc, "SELECT COALESCE(COUNT(*), 0) AS n, COALESCE(MIN(year), 0) AS year_min, COALESCE(MAX(year), 0) AS year_max FROM n" + tblName + " WHERE data_file_id = " + df.id + ";", function (err, res) {
  dbi.execQry(dbc, "SELECT COALESCE(COUNT(*), 0) AS n, COALESCE(MIN(year), 0) AS year_min, COALESCE(MAX(year), 0) AS year_max FROM " + tblName + ";", function (err, res) {
    if (!err) {
      dbi.execQry(dbc, "UPDATE data_file SET n_ngram_imp = " + res.rows[0].n + ", year_min = " + res.rows[0].year_min + ", year_max = " + res.rows[0].year_max + ", is_upd = 1 WHERE id = " + df.id + ";", fnNext);
    }
    else fnNext(err, res);
  });
}


// ---------------------------------------------------------------------------------------------------------------------
function dbWipe(lang) {
  utilGetUserConfirm("Type 'WIPE' to wipe the data for language '" + lang + "':", "wipe",
    function () {
      timer.start("dbWipe");
      var dbc = dbi.open(true);
      dbi.execQry(dbc, "SET search_path TO \"" + lang + "\"; TRUNCATE data_file, n3*, n4*, n5* RESTART IDENTITY CASCADE;", function (err, res) {
        dbi.close(dbc, true, true, null, function () {
          if (!err) {
            console.log("Data wiped.");
            console.log("Time elapsed: " + timer.end("dbWipe", true) + " (" + timer.end("dbWipe") + "ms)");
          }
        });
      });
    },
    function () {
      console.log("Data wipe canceled.");
    }
  );
}


// ---------------------------------------------------------------------------------------------------------------------
function isLangValid(lang, res) {
  if (LANG.indexOf(lang) === -1) {
    var msg = "Invalid language specified. Valid languages list: " + LANG.join(", ") + ".";
    (res ? serverSend(res, msg) : console.log(msg));
    return false;
  }
  else return true;
}


// ---------------------------------------------------------------------------------------------------------------------
/**
 * Is the word 'w' all part-of-speech (e.g., "_ADJ_")?
 */
function isWordAllPOS(w) {
  for (var i = 0; i < POS.length; i++) {
    if (w == ("_" + POS[i] + "_")) return true;
  }
  return false;
}


// ---------------------------------------------------------------------------------------------------------------------
function log(filename, addTimestamp, fields, delimiter) {
  if (fields.length === 0) return;

  if (!fs.existsSync(DIR_LOG)) fs.mkdirSync(DIR_LOG);
  fs.appendFileSync(DIR_LOG + "/" + filename, (addTimestamp ? moment().format() + (delimiter || "\t") : "") + fields.join(delimiter || "\t") + "\n");
}


// ---------------------------------------------------------------------------------------------------------------------
/**
 *
 * TODO:
 *   - exclude all POS words when quering for words as they will scew the results (or will they?)
 *
 * E.g.: node do.js pred-get spa 3 4 0 0 "Ngo Dinh Diem"
 */
function predGet(lang, n, decCnt, year01, year02, words, res) {
  if (!res) timer.start("predGet");

  var wordsSQL = [];
  for (var i=0; i < n; i++) {
    //wordsSQL.push("w" + (i+1) + " LIKE '" + words[i] + "%'");
    wordsSQL.push("w" + (i+1) + " = '" + words[i] + "'");
  }

  var letters = words[0].substr(0,2).toLowerCase();
  if (LETTERS.indexOf(letters) === -1) return console.log("Letters '" + letters + "' not recognized");

  var qry =
    "SELECT ROUND(" +
    "(SELECT SUM(n) FROM n" + n + "_" + letters + " WHERE " + (year01 !== 0 ? "year >= " + year01 + " AND " : "") + (year02 !== 0 ? "year <= " + year02 + " AND " : "") + wordsSQL.join(" AND ") + ") / " +
    "(SELECT SUM(n) FROM n" + n + "_" + letters + " WHERE " + (year01 !== 0 ? "year >= " + year01 + " AND " : "") + (year02 !== 0 ? "year <= " + year02 + " AND " : "") + wordsSQL.slice(0, n-1).join(" AND ") +
    ")::numeric, " + decCnt + ") AS cp;";
  //console.log(qry);

  var dbc = dbi.open(false);
  //dbi.execQry(dbc, "SET search_path TO \"" + lang + "\"; " + qry, function (result) {
  dbi.execQry(dbc, "SET search_path to \"" + lang + "\"; " + qry, function (err, result) {
    var cp = (result ? result.rows[0].cp : null);

    var t = timer.end("predGet");
    var msg = (cp === null ? -1 + " ngram not found" : cp) + (!res ? " (" + timer.msToTime(t) + ", " + t + "ms)" : "");
    (res ? serverSend(res, msg) : console.log(msg));

    dbi.close(dbc, false);
  });
}


// ---------------------------------------------------------------------------------------------------------------------
// node do.js pred-get-microsoft eng 3 "one two three" 1
function predGetMS(lang, n, words) {
  if (isMSQueryRunning) return console.log("null (another microsoft query already running)");
  if (lang !== "eng") return console.log("null (only english is supported)");

  isMSQueryRunning = true;
  request("http://weblm.research.microsoft.com/rest.svc/bing-body/2013-12/" + n + "/cp?u=" + PRIVATE.MICROSOFT_USER_TOKEN + "&p=" + words.join("+"), function (err, httpResponse, body) {
    if (err) console.log(-2);
    console.log(body);
    isMSQueryRunning = false;
  });
}


// ---------------------------------------------------------------------------------------------------------------------
function pushover(title, body, priority) {
  if (!PUSHOVER_USR_KEY || PUSHOVER_USR_KEY.length === 0 || !PUSHOVER_APP_KEY || PUSHOVER_APP_KEY.length === 0) return;

  if (os.hostname() !== PUSHOVER_HOST) return;

  var p = new push({ user: PUSHOVER_USR_KEY, token: PUSHOVER_APP_KEY });
  p.send({ title: title, message: (!body || body.length === 0 ? null : body), device: PUSHOVER_DEV, sound: PUSHOVER_SND, priority: priority });
}


// ---------------------------------------------------------------------------------------------------------------------
/**
 * RES:
 *   stackoverflow.com/questions/19593976/node-js-server-start-and-stop
 *   localhost:10000/get-pred?lang=eng&n=3&dec-num-cnt=4&year-01=0&year-02=0&ngram=a+b+c
 *   localhost:10000/get-pred?lang=spa&n=3&dec-num-cnt=4&year-01=0&year-02=0&ngram=Ngo+Dinh+Diem
 */
function serverStart() {
  http.createServer(function (req, res) {
    var up = url.parse(req.url);
    var qs = utilGetQS(up.search);

    switch (up.pathname) {
      case "/get-pred":
        if (qs["lang"] === undefined || qs["n"] === undefined || qs["dec-num-cnt"] === undefined || qs["year-01"] == undefined || qs["year-02"] === undefined || qs["ngram"] === undefined) return serverSend(res, "-2 Required query string arguments: lang, n, dec-num-cnt, year-01, year-02, ngram");

        qs["n"]           = parseInt(qs["n"]);
        qs["dec-num-cnt"] = parseInt(qs["dec-num-cnt"]);
        qs["year-01"]     = parseInt(qs["year-01"]);
        qs["year-02"]     = parseInt(qs["year-02"]);

        var words = qs["ngram"].split("+");

        if (!isLangValid(qs["lang"], res))                      return;
        if (isNaN(qs["n"]) || qs["n"] < 3 || qs["n"] > 5)       return serverSend(res, "-2 'n' needs to parseable to an integer between 3 and 5.");
        if (isNaN(qs["dec-num-cnt"]) || qs["dec-num-cnt"] <= 0) return serverSend(res, "-2 'dec-num-cnt' needs to be parseable to a positive integer.");
        if (isNaN(qs["year-01"]))                               return serverSend(res, "-2 'year-01' needs to be parseable to an integer between " + YEAR_MIN + " and " + YEAR_MAX + ".");
        if (isNaN(qs["year-02"]))                               return serverSend(res, "-2 'year-02' needs to be parseable to an integer between " + YEAR_MIN + " and " + YEAR_MAX + ".");
        if (qs["year-01"] > qs["year-02"])                      return serverSend(res, "-2 'year-01' needs to be less of equal to 'year-02.'");
        if (words.length !== qs["n"])                           return serverSend(res, "-2 The number of ngrams does not match the 'n' provided. Words in an ngram should be space-delimited.");

        //predGet(qs["lang"], qs["n"], qs["dec-num-cnt"], qs["year-01"], qs["year-02"], words, res);
        //serverSend(res, "" + words.length);
        serverSend(res, "" + 0.05);
        break;
      default:
        //console.log(up.pathname.substr(1) + (up.search ? up.search : ""));
        serverSend(res, "Unknown resource requested.");
    }

    log("http.log", true, [ req.connection.remoteAddress, req.url.substr(1) ]);

    //process.exit(0);
  }).listen(SERVER_PORT);

  console.log("NGram server running on port " + SERVER_PORT);
}


// ---------------------------------------------------------------------------------------------------------------------
function serverStatus() {}


// ---------------------------------------------------------------------------------------------------------------------
function serverStop() {}


// ---------------------------------------------------------------------------------------------------------------------
function serverSend(res, s) {
  res.writeHead(200, { "Content-Type": "text/plain" });
  res.write(s);
  res.end();
}


// ---------------------------------------------------------------------------------------------------------------------
function utilBytesToSize(bytes) {
  var sizes = [ "", "k", "M", "G", "T" ];
  if (bytes === 0) return "0B";

  var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
  if (i == 0) return bytes + sizes[i];
  return (bytes / Math.pow(1024, i)).toFixed(2) + sizes[i];
}


// ---------------------------------------------------------------------------------------------------------------------
function utilGetFileSize(filepath, doHumanReadable) {
  var s = fs.statSync(filepath).size;
  return (doHumanReadable ? utilBytesToSize(s) : s);
}


// ---------------------------------------------------------------------------------------------------------------------
function utilGetLineCnt(filepath) {
  return parseInt(sh.exec("wc -l " + filepath, { silent: true }).output);
}


// ---------------------------------------------------------------------------------------------------------------------
function utilGetQS(s) {
  if (!s || s.length === 0) return {};

  var qs = {};

  var A = s.substr(1).split("&");
  for (var i=0, ni=A.length; i < ni; i++) {
    var B = A[i].split("=");
    qs[B[0]] = B[1];
  }

  return qs;
}


// ---------------------------------------------------------------------------------------------------------------------
function utilGetUserConfirm(prompt, confirmStr, fnY, fnN, doAutoConfirm) {
  if (doAutoConfirm) return fnY();

  var rl = readline.createInterface({ input: process.stdin, output: process.stdout });
  rl.question(prompt + " ", function(answer) {
    if (answer.toLowerCase() === confirmStr) fnY();
    else fnN();
    rl.close();
  });
}


// ---------------------------------------------------------------------------------------------------------------------
function main() {
  var cmd = process.argv[2];

  switch (cmd) {
    case "data-download-all":
      if (process.argv.length <= 3) return console.log("Usage: node " + progName + " " + cmd + " <language>");

      var lang = process.argv[3];
      if (!isLangValid(lang)) return;

      timer.start("dataDownloadAll");
      dataDownloadOne(lang, 3, 0, true, function (timeSum, sizeSum) {
        console.log("Total size: " + utilBytesToSize(sizeSum));
        console.log("Elapsed time: " + timer.end("dataDownloadAll", true) + " (" + timer.end("dataDownloadAll") + "ms)");

        pushover("Download complete", "All done (" + utilBytesToSize(sizeSum) + ", " + timer.end("dataDownloadAll", true) + ")", 1);
      });
      break;

    case "data-download-one":
      if (process.argv.length <= 5) return console.log("Usage: node " + progName + " " + cmd + " <language> <n> <letters>");

      var lang = process.argv[3];
      var n            = parseInt(process.argv[4]);
      var lettersIdx   = LETTERS.indexOf(process.argv[5]);

      if (!isLangValid(lang)) return;
      if (isNaN(n) || n < 3 || n > 5) return console.log("Error. 'n' needs to parseable to an integer between 3 and 5.");
      if (lettersIdx === -1)          return console.log("Error. 'letters' need to be the two first letters of a Google Ngram file.");

      dataDownloadOne(lang, n, lettersIdx, false, null);
      break;

    case "data-get-size":
      if (process.argv.length <= 3) return console.log("Usage: node " + progName + " " + cmd + " <language>");

      var lang = process.argv[3];
      if (!isLangValid(lang)) return;

      dataGetSizeAll(lang);
      break;

    case "db-analyze":
      if (process.argv.length <= 4) return console.log("Usage: node " + progName + " " + cmd + " <language> <n>");

      var lang = process.argv[3];
      var n    = parseInt(process.argv[4]);

      if (!isLangValid(lang)) return;
      if (isNaN(n) || n < 3 || n > 5) return console.log("Error. 'n' needs to parseable to an integer between 3 and 5.");

      dbAnalyze(lang, n);
      break;

    case "db-get-inf":
      if (process.argv.length <= 3) return console.log("Usage: node " + progName + " " + cmd + " <language>");

      var lang = process.argv[3];
      if (!isLangValid(lang)) return;

      dbGetInf(lang);
      break;

    case "db-get-inf-per-file":
      if (process.argv.length <= 3) return console.log("Usage: node " + progName + " " + cmd + " <language>");

      var lang = process.argv[3];
      if (!isLangValid(lang)) return;

      dbGetInfPerFile(lang);
      break;

    case "db-index-add":
      if (process.argv.length <= 4) return console.log("Usage: node " + progName + " " + cmd + " <language> <n>");

      var lang = process.argv[3];
      var n    = parseInt(process.argv[4]);

      if (!isLangValid(lang)) return;
      if (isNaN(n) || n < 3 || n > 5) return console.log("Error. 'n' needs to parseable to an integer between 3 and 5.");

      dbIndexAdd(lang, n);
      break;

    case "db-init":
      dbInit();
      break;

    case "db-index-del":
      if (process.argv.length <= 4) return console.log("Usage: node " + progName + " " + cmd + " <language> <n>");

      var lang = process.argv[3];
      var n    = parseInt(process.argv[4]);

      if (!isLangValid(lang)) return;
      if (isNaN(n) || n < 3 || n > 5) return console.log("Error. 'n' needs to parseable to an integer between 3 and 5.");

      dbIndexDel(lang, n);
      break;

    case "db-init":
      dbInit();
      break;

    case "db-populate-all":
      if (process.argv.length <= 10) return console.log("Usage: node " + progName + " " + cmd + " <language> <n> <exc-word-with-numbers?> <exc-words-with-punctuation?> <exc-all-caps?> <exc-all-part-of-speech?> <remove-data?> <auto-confirm?>");

      var lang          = process.argv[3];
      var n             = parseInt(process.argv[4]);
      var doExcNum      = (process.argv [5] == 1);
      var doExcPunct    = (process.argv [6] == 1);
      var doExcAllCaps  = (process.argv [7] == 1);
      var doExcAllPOS   = (process.argv [8] == 1);
      var doRemoveData  = (process.argv [9] == 1);
      var doAutoConfirm = (process.argv[10] == 1);

      if (!isLangValid(lang)) return;

      utilGetUserConfirm("Type 'POP' to populate the data for language '" + lang + "' (existing data for that language will be replaced):", "pop",
        function () { dbPopulate(lang, n, 0, doExcNum, doExcPunct, doExcAllCaps, doExcAllPOS, doRemoveData, true); },
        function () { console.log("Database population canceled."); },
        doAutoConfirm
      );
      break;

    case "db-populate-one":  // node do.js db-populate-one eng 3 ng 1 1 1 1; node do.js db-get-inf eng
      if (process.argv.length <= 11) return console.log("Usage: node " + progName + " " + cmd + " <language> <n> <letters> <exc-word-with-numbers?> <exc-words-with-punctuation?> <exc-all-caps?> <exc-all-part-of-speech?> <remove-data?> <auto-confirm?>");

      var lang          = process.argv[3];
      var n             = parseInt(process.argv[4]);
      var lettersIdx    = LETTERS.indexOf(process.argv[5]);
      var doExcNum      = (process.argv [6] == 1);
      var doExcPunct    = (process.argv [7] == 1);
      var doExcAllCaps  = (process.argv [8] == 1);
      var doExcAllPOS   = (process.argv [9] == 1);
      var doRemoveData  = (process.argv[10] == 1);
      var doAutoConfirm = (process.argv[11] == 1);

      if (!isLangValid(lang)) return;
      if (isNaN(n) || n < 3 || n > 5) return console.log("Error. 'n' needs to parseable to an integer between 3 and 5.");
      if (lettersIdx === -1)          return console.log("Error. 'letters' need to be the two first letters of a Google Ngram file.");

      utilGetUserConfirm("Type 'pop' to populate the data for language '" + lang + "' (existing data for that language will be replaced):", "pop",
        function () {
          dbPopulate(lang, n, lettersIdx, false, doExcNum, doExcPunct, doExcAllCaps, doExcAllPOS, doRemoveData, false);
        },
        function () { console.log("Database population canceled."); },
        doAutoConfirm
      );
      break;

    case "db-refactor-1":
      if (process.argv.length <= 4) return console.log("Usage: node " + progName + " " + cmd + " <language> <auto-confirm?>");

      var lang          = process.argv[3];
      var doAutoConfirm = (process.argv[4] == 1);

      if (!isLangValid(lang)) return;

      utilGetUserConfirm("Type 'refact' to refactor the database (phase 1) for language '" + lang + "':", "refact",
        function () { dbRefactor01(lang); },
        function () { console.log("Refactring database (phase 1) canceled."); },
        doAutoConfirm
      );
      break;

    case "db-refactor-2":
      if (process.argv.length <= 4) return console.log("Usage: node " + progName + " " + cmd + " <language> <auto-confirm?>");

      var lang          = process.argv[3];
      var doAutoConfirm = (process.argv[4] == 1);

      if (!isLangValid(lang)) return;

      utilGetUserConfirm("Type 'refact' to refactor the database (phase 2) for language '" + lang + "':", "refact",
        function () { dbRefactor02(lang); },
        function () { console.log("Refactring database (phase 2) canceled."); },
        doAutoConfirm
      );
      break;

    case "db-refactor-3":
      if (process.argv.length <= 6) return console.log("Usage: node " + progName + " " + cmd + " <language> <n> <word-num> <auto-confirm?>");

      var lang          = process.argv[3];
      var n             = parseInt(process.argv[4]);
      var wordNum       = parseInt(process.argv[5]);
      var doAutoConfirm = (process.argv[6] == 1);

      if (!isLangValid(lang)) return;
      if (isNaN(n) || n < 3 || n > 5)                   return console.log("Error. 'n' needs to parseable to an integer between 3 and 5.");
      if (isNaN(wordNum) || wordNum < 1 || wordNum > n) return console.log("Error. 'word-num' needs to parseable to an integer between 1 and n (i.e., " + n + " here).");

      utilGetUserConfirm("Type 'refact' to refactor the database (phase 3) for language '" + lang + "':", "refact",
        function () { dbRefactor03(lang, n, wordNum); },
        function () { console.log("Refactring database (phase 3) canceled."); },
        doAutoConfirm
      );
      break;

    case "db-refactor-4":
      break;

    case "db-refactor-5":
      break;

    case "db-refactor-6":
      break;

    case "db-wipe":
      if (process.argv.length <= 3) return console.log("Usage: node " + progName + " " + cmd + " <language>");

      var lang = process.argv[3];
      if (!isLangValid(lang)) return;

      dbWipe(lang);
      break;

    case "help":
      console.log("This command line utility manages the Ngram application, from downloading Google Ngram corpus, to initializing the database, to populating that database with the downloaded data, to calculating word predictability, to starting an HTTP server that provides that predictability remotely. Contact Tomek Loboda (tol7@pitt.edu) for more information.");
      break;

    case "ping":
      console.log("pong");
      break;

    case "pred-get":  // node do.js pred-get eng 3 4 0 0 "awarded for that"
      if (process.argv.length <= 8) return console.log("Usage: node " + progName + " " + cmd + " <language> <n> <dec-num-cnt> <year-01> <year-02> <ngram>");

      var lang   = process.argv[3];
      var n      = parseInt(process.argv[4]);
      var decCnt = parseInt(process.argv[5]);
      var year01 = parseInt(process.argv[6]);
      var year02 = parseInt(process.argv[7]);
      var words  = process.argv[8].split(/\s+/);

      if (!isLangValid(lang)) return;
      if (isNaN(n) || n < 3 || n > 5)                      return console.log("Error. 'n' needs to parseable to an integer between 3 and 5.");
      if (isNaN(decCnt) || decCnt <= 0)                    return console.log("Error. 'dec-num-cnt' needs to be parseable to a positive integer.");
      if (isNaN(year01))                                   return console.log("Error. 'year' needs to be parseable to an integer between " + YEAR_MIN + " and " + YEAR_MAX + ".");
      if (isNaN(year02))                                   return console.log("Error. 'year' needs to be parseable to an integer between " + YEAR_MIN + " and " + YEAR_MAX + ".");
      if (year01 !== 0 && year02 !== 0 && year01 > year02) return console.log("Error. 'year-01' needs to be less of equal to 'year-02.'");
      if (words.length !== n)                              return console.log("Error. The number of ngrams does not match the 'n' provided. Words in an ngram should be space-delimited.");

      predGet(lang, n, decCnt, year01, year02, words);
      break;

    case "pred-get-microsoft":
      if (process.argv.length <= 5) return console.log("Usage: node " + progName + " " + cmd + " <language> <n> <ngram>");

      var lang  = process.argv[3];
      var n     = parseInt(process.argv[4]);
      var words = process.argv[5].split(/\s+/);

      if (!isLangValid(lang)) return;
      if (isNaN(n) || n < 3 || n > 5) return console.log("Error. 'n' needs to parseable to an integer between 3 and 5.");
      if (words.length !== n)         return console.log("Error. The number of ngrams does not match the 'n' provided. Words in an ngram should be space-delimited.");

      predGetMS(lang, n, words);
      break;

    case "server-start":
      serverStart();
      break;

    case "server-status":
      serverStatus();
      break;

    case "server-stop":
      serverStop();
      break;

    default:
      console.log("Usage: node " + progName + " command")
      console.log("Available commands: data-download-all, data-download-one, data-get-size, db-analyze, db-get-inf, db-get-inf-per-file, db-index-add, db-index-del, db-init, db-populate-all, db-populate-one, db-refactor, db-wipe, help, ping, pred-get, pred-get-microsoft, server-start, server-status, server-stop");
  }
}


// ---------------------------------------------------------------------------------------------------------------------
main();


// ---------------------------------------------------------------------------------------------------------------------
/*
console.log(utilBytesToSize(127677575340));
console.log(timer.msToTime(340043031));


parallel --no-notice --max-procs 4 node do.js db-populate-one eng 3 {} 1 0 0 0 0 1 ::: a_ aa ab ac ad ae af ag ah ai aj ak al am an ao ap aq ar as at au av aw ax ay az
parallel --no-notice --max-procs 4 node do.js db-populate-one eng 3 {} 1 0 0 0 0 1 ::: b_ ba bb bc bd be bf bg bh bi bj bk bl bm bn bo bp bq br bs bt bu bv bw bx by bz
parallel --no-notice --max-procs 4 node do.js db-populate-one eng 3 {} 1 0 0 0 0 1 ::: c_ ca cb cc cd ce cf cg ch ci cj ck cl cm cn co cp cq cr cs ct cu cv cw cx cy cz
parallel --no-notice --max-procs 4 node do.js db-populate-one eng 3 {} 1 0 0 0 0 1 ::: d_ da db dc dd de df dg dh di dj dk dl dm dn do dp dq dr ds dt du dv dw dx dy dz
parallel --no-notice --max-procs 4 node do.js db-populate-one eng 3 {} 1 0 0 0 0 1 ::: e_ ea eb ec ed ee ef eg eh ei ej ek el em en eo ep eq er es et eu ev ew ex ey ez
parallel --no-notice --max-procs 4 node do.js db-populate-one eng 3 {} 1 0 0 0 0 1 ::: f_ fa fb fc fd fe ff fg fh fi fj fk fl fm fn fo fp fq fr fs ft fu fv fw fx fy fz
parallel --no-notice --max-procs 4 node do.js db-populate-one eng 3 {} 1 0 0 0 0 1 ::: g_ ga gb gc gd ge gf gg gh gi gj gk gl gm gn go gp gq gr gs gt gu gv gw gx gy gz
parallel --no-notice --max-procs 4 node do.js db-populate-one eng 3 {} 1 0 0 0 0 1 ::: h_ ha hb hc hd he hf hg hh hi hj hk hl hm hn ho hp hq hr hs ht hu hv hw hx hy hz
parallel --no-notice --max-procs 4 node do.js db-populate-one eng 3 {} 1 0 0 0 0 1 ::: i_ ia ib ic id ie if ig ih ii ij ik il im in io ip iq ir is it iu iv iw ix iy iz
parallel --no-notice --max-procs 4 node do.js db-populate-one eng 3 {} 1 0 0 0 0 1 ::: j_ ja jb jc jd je jf jg jh ji jj jk jl jm jn jo jp jq jr js jt ju jv jw jx jy jz
parallel --no-notice --max-procs 4 node do.js db-populate-one eng 3 {} 1 0 0 0 0 1 ::: k_ ka kb kc kd ke kf kg kh ki kj kk kl km kn ko kp kq kr ks kt ku kv kw kx ky kz
parallel --no-notice --max-procs 4 node do.js db-populate-one eng 3 {} 1 0 0 0 0 1 ::: l_ la lb lc ld le lf lg lh li lj lk ll lm ln lo lp lq lr ls lt lu lv lw lx ly lz
parallel --no-notice --max-procs 4 node do.js db-populate-one eng 3 {} 1 0 0 0 0 1 ::: m_ ma mb mc md me mf mg mh mi mj mk ml mm mn mo mp mq mr ms mt mu mv mw mx my mz
parallel --no-notice --max-procs 4 node do.js db-populate-one eng 3 {} 1 0 0 0 0 1 ::: n_ na nb nc nd ne nf ng nh ni nj nk nl nm nn no np nq nr ns nt nu nv nw nx ny nz
parallel --no-notice --max-procs 4 node do.js db-populate-one eng 3 {} 1 0 0 0 0 1 ::: o_ oa ob oc od oe of og oh oi oj ok ol om on oo op oq or os ot ou ov ow ox oy oz
parallel --no-notice --max-procs 4 node do.js db-populate-one eng 3 {} 1 0 0 0 0 1 ::: p_ pa pb pc pd pe pf pg ph pi pj pk pl pm pn po pp pq pr ps pt pu pv pw px py pz
parallel --no-notice --max-procs 4 node do.js db-populate-one eng 3 {} 1 0 0 0 0 1 ::: q_ qa qb qc qd qe qf qg qh qi qj qk ql qm qn qo qp qq qr qs qt qu qv qw qx qy qz
parallel --no-notice --max-procs 4 node do.js db-populate-one eng 3 {} 1 0 0 0 0 1 ::: r_ ra rb rc rd re rf rg rh ri rj rk rl rm rn ro rp rq rr rs rt ru rv rw rx ry rz
parallel --no-notice --max-procs 4 node do.js db-populate-one eng 3 {} 1 0 0 0 0 1 ::: s_ sa sb sc sd se sf sg sh si sj sk sl sm sn so sp sq sr ss st su sv sw sx sy sz
parallel --no-notice --max-procs 4 node do.js db-populate-one eng 3 {} 1 0 0 0 0 1 ::: t_ ta tb tc td te tf tg th ti tj tk tl tm tn to tp tq tr ts tt tu tv tw tx ty tz
parallel --no-notice --max-procs 4 node do.js db-populate-one eng 3 {} 1 0 0 0 0 1 ::: u_ ua ub uc ud ue uf ug uh ui uj uk ul um un uo up uq ur us ut uu uv uw ux uy uz
parallel --no-notice --max-procs 4 node do.js db-populate-one eng 3 {} 1 0 0 0 0 1 ::: v_ va vb vc vd ve vf vg vh vi vj vk vl vm vn vo vp vq vr vs vt vu vv vw vx vy vz
parallel --no-notice --max-procs 4 node do.js db-populate-one eng 3 {} 1 0 0 0 0 1 ::: w_ wa wb wc wd we wf wg wh wi wj wk wl wm wn wo wp wq wr ws wt wu wv ww wx wy wz
parallel --no-notice --max-procs 4 node do.js db-populate-one eng 3 {} 1 0 0 0 0 1 ::: x_ xa xb xc xd xe xf xg xh xi xj xk xl xm xn xo xp xq xr xs xt xu xv xw xx xy xz
parallel --no-notice --max-procs 4 node do.js db-populate-one eng 3 {} 1 0 0 0 0 1 ::: y_ ya yb yc yd ye yf yg yh yi yj yk yl ym yn yo yp yq yr ys yt yu yv yw yx yy yz
parallel --no-notice --max-procs 4 node do.js db-populate-one eng 3 {} 1 0 0 0 0 1 ::: z_ za zb zc zd ze zf zg zh zi zj zk zl zm zn zo zp zq zr zs zt zu zv zw zx zy zz
parallel --no-notice --max-procs 4 node do.js db-populate-one eng 3 {} 1 0 0 0 0 1 ::: _ADJ_ _ADP_ _ADV_ _CONJ_ _DET_ _NOUN_ _NUM_ _PRON_ _PRT_ _VERB_ other punctuation

parallel --no-notice --max-procs 4 node do.js db-populate-one eng 5 {} 1 0 0 0 0 1 ::: a_ aa ab ac ad ae af ag ah ai aj ak al am an ao ap aq ar as at au av aw ax ay az b_ ba bb bc bd be bf bg bh bi bj bk bl bm bn bo bp bq br bs bt bu bv bw bx by bz c_ ca cb cc cd ce cf cg ch ci cj ck cl cm cn co cp cq cr cs ct cu cv cw cx cy cz d_ da db dc dd de df dg dh di dj dk dl dm dn do dp dq dr ds dt du dv dw dx dy dz e_ ea eb ec ed ee ef eg eh ei ej ek el em en eo ep eq er es et eu ev ew ex ey ez f_ fa fb fc fd fe ff fg fh fi fj fk fl fm fn fo fp fq fr fs ft fu fv fw fx fy fz g_ ga gb gc gd ge gf gg gh gi gj gk gl gm gn go gp gq gr gs gt gu gv gw gx gy gz h_ ha hb hc hd he hf hg hh hi hj hk hl hm hn ho hp hq hr hs ht hu hv hw hx hy hz i_ ia ib ic id ie if ig ih ii ij ik il im in io ip iq ir is it iu iv iw ix iy iz j_ ja jb jc jd je jf jg jh ji jj jk jl jm jn jo jp jq jr js jt ju jv jw jx jy jz k_ ka kb kc kd ke kf kg kh ki kj kk kl km kn ko kp kq kr ks kt ku kv kw kx ky kz l_ la lb lc ld le lf lg lh li lj lk ll lm ln lo lp lq lr ls lt lu lv lw lx ly lz m_ ma mb mc md me mf mg mh mi mj mk ml mm mn mo mp mq mr ms mt mu mv mw mx my mz n_ na nb nc nd ne nf ng nh ni nj nk nl nm nn no np nq nr ns nt nu nv nw nx ny nz o_ oa ob oc od oe of og oh oi oj ok ol om on oo op oq or os ot ou ov ow ox oy oz p_ pa pb pc pd pe pf pg ph pi pj pk pl pm pn po pp pq pr ps pt pu pv pw px py pz q_ qa qb qc qd qe qf qg qh qi qj qk ql qm qn qo qp qq qr qs qt qu qv qw qx qy qz r_ ra rb rc rd re rf rg rh ri rj rk rl rm rn ro rp rq rr rs rt ru rv rw rx ry rz s_ sa sb sc sd se sf sg sh si sj sk sl sm sn so sp sq sr ss st su sv sw sx sy sz t_ ta tb tc td te tf tg th ti tj tk tl tm tn to tp tq tr ts tt tu tv tw tx ty tz u_ ua ub uc ud ue uf ug uh ui uj uk ul um un uo up uq ur us ut uu uv uw ux uy uz v_ va vb vc vd ve vf vg vh vi vj vk vl vm vn vo vp vq vr vs vt vu vv vw vx vy vz w_ wa wb wc wd we wf wg wh wi wj wk wl wm wn wo wp wq wr ws wt wu wv ww wx wy wz x_ xa xb xc xd xe xf xg xh xi xj xk xl xm xn xo xp xq xr xs xt xu xv xw xx xy xz y_ ya yb yc yd ye yf yg yh yi yj yk yl ym yn yo yp yq yr ys yt yu yv yw yx yy yz z_ za zb zc zd ze zf zg zh zi zj zk zl zm zn zo zp zq zr zs zt zu zv zw zx zy zz >> log/tmp

_ADJ_ _ADP_ _ADV_ _CONJ_ _DET_ _NOUN_ _NUM_ _PRON_ _PRT_ _VERB_ other punctuation
*/
